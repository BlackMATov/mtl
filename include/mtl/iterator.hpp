#pragma once

#include <mtl/internal/base.hpp>

namespace mtl
{
	// -------------------------------------------------------------------------
	//
	// iterator
	//
	// -------------------------------------------------------------------------
	
	template
	<
		typename Type,
		typename Pointer,
		typename Reference
	>
	struct iterator{
		typedef Type      value_type;
		typedef Pointer   pointer;
		typedef Reference reference;
	};
	
	// -------------------------------------------------------------------------
	//
	// iterator_traits
	//
	// -------------------------------------------------------------------------
	
	template < typename Iterator >
	struct iterator_traits{
		typedef typename Iterator::value_type value_type;
		typedef typename Iterator::pointer    pointer;
		typedef typename Iterator::reference  reference;
	};
	
	template < typename T >
	struct iterator_traits<T*>{
		typedef T        value_type;
		typedef T*       pointer;
		typedef T&       reference;
	};
	
	template < typename T >
	struct iterator_traits<const T*>{
		typedef T        value_type;
		typedef const T* pointer;
		typedef const T& reference;
	};
	
	// -------------------------------------------------------------------------
	//
	// reverse_iterator
	//
	// -------------------------------------------------------------------------
	
	template < typename Iterator >
	class reverse_iterator : public iterator
	<
		typename iterator_traits<Iterator>::value_type,
		typename iterator_traits<Iterator>::pointer,
		typename iterator_traits<Iterator>::reference
	>{
	private:
		Iterator _iterator;
	public:
		typedef Iterator iterator_type;
		typedef typename iterator_traits<Iterator>::pointer   pointer;
		typedef typename iterator_traits<Iterator>::reference reference;
		
		reverse_iterator()
		: _iterator() {}
		
		reverse_iterator(const reverse_iterator& other)
		: _iterator(other.base()) {}
		
		reverse_iterator& operator=(const reverse_iterator& other){
			if ( this != &other ) {
				_iterator = other.base();
			}
			return *this;
		}
		
		explicit reverse_iterator(iterator_type iterator)
		: _iterator(iterator) {}
		
		template < typename U >
		reverse_iterator(const reverse_iterator<U>& other)
		: _iterator(other.base()) {}
		
		template < typename U >
		reverse_iterator& operator=(const reverse_iterator<U>& other){
			_iterator = other.base();
			return *this;
		}
		
		iterator_type base() const{
			return _iterator;
		}
		
		reference operator*() const{
			iterator_type i(_iterator);
			return *--i;
		}
		
		pointer operator->() const{
			return &(operator*());
		}
		
		reverse_iterator& operator++(){
			--_iterator;
			return *this;
		}
		
		reverse_iterator& operator--(){
			++_iterator;
			return *this;
		}
		
		reverse_iterator operator++(int){
			reverse_iterator tmp(*this);
			--_iterator;
			return tmp;
		}
		
		reverse_iterator operator--(int){
			reverse_iterator tmp(*this);
			++_iterator;
			return tmp;
		}
		
		reverse_iterator operator+(ptrdiff_t n) const{
			return reverse_iterator(_iterator - n);
		}
		
		reverse_iterator operator-(ptrdiff_t n) const{
			return reverse_iterator(_iterator + n);
		}
		
		reverse_iterator& operator+=(ptrdiff_t n){
			_iterator -= n;
			return *this;
		}
		
		reverse_iterator& operator-=(ptrdiff_t n){
			_iterator += n;
			return *this;
		}
		
		reference operator[](ptrdiff_t n) const{
			return _iterator[-n - 1];
		}
	};
	
	// -------------------------------------------------------------------------
	//
	// reverse_iterator operators
	//
	// -------------------------------------------------------------------------
	
	template < typename Iterator1, typename Iterator2 >
	inline bool operator==(
		const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() == rhs.base();
	}
	
	template < typename Iterator1, typename Iterator2 >
	inline bool operator!=(
		const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() != rhs.base();
	}
	
	template < typename Iterator1, typename Iterator2 >
	inline bool operator<(
		const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() > rhs.base();
	}
	
	template < typename Iterator1, typename Iterator2 >
	inline bool operator<=(
		const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() >= rhs.base();
	}
	
	template < typename Iterator1, typename Iterator2 >
	inline bool operator>(
		const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() < rhs.base();
	}
	
	template < typename Iterator1, typename Iterator2 >
	inline bool operator>=(
		const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return lhs.base() <= rhs.base();
	}
	
	template < typename Iterator1, typename Iterator2 >
	inline ptrdiff_t operator-(
		const reverse_iterator<Iterator1>& lhs,
		const reverse_iterator<Iterator2>& rhs)
	{
		return rhs.base() - lhs.base();
	}
	
	template < typename Iterator >
	inline reverse_iterator<Iterator> operator+(
		ptrdiff_t n, const reverse_iterator<Iterator>& a)
	{
		return reverse_iterator<Iterator>(a.base() - n);
	}
} // namespace mtl
