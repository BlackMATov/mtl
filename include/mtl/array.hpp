#pragma once

#include <mtl/allocator.hpp>
#include <mtl/algorithm.hpp>
#include <mtl/internal/algo.hpp>

/// \todo template push_back and etc
/// \todo add allocator_traits

namespace mtl
{
	// -------------------------------------------------------------------------
	// 
	// array_base
	// 
	// -------------------------------------------------------------------------
	
	template < typename T, typename A >
	class array_base{
		array_base();
		array_base(const array_base&);
		array_base& operator=(const array_base&);
	protected:
		T*     _first;
		T*     _last;
		T*     _capacity;
		A      _allocator;
	protected:
		T*     _do_alloc(size_t n);
		void   _do_dealloc(T* data, size_t n);
		void   _do_alloc_self(size_t n);
		void   _do_dealloc_self();
		void   _do_realloc_self(size_t new_capacity);
		size_t _next_capacity(size_t minimum);
	public:
		array_base(const A& allocator);
		~array_base();
	};
	
	// -------------------------------------------------------------------------
	// 
	// array_base impl
	// 
	// -------------------------------------------------------------------------
	
	template < typename T, typename A >
	array_base<T,A>::array_base(const A& allocator)
	: _first(NULL), _last(NULL), _capacity(NULL)
	, _allocator(allocator){
	}
	
	template < typename T, typename A >
	array_base<T,A>::~array_base(){
		_do_dealloc_self();
	}
	
	template < typename T, typename A >
	T* array_base<T,A>::_do_alloc(size_t n){
		MTL_ASSERT(n > 0);
		const size_t size = sizeof(T) * n;
		return static_cast<T*>(_allocator.allocate(size));
	}
	
	template < typename T, typename A >
	void array_base<T,A>::_do_dealloc(T* data, size_t n){
		MTL_ASSERT(data && n > 0);
		const size_t size = sizeof(T) * n;
		_allocator.deallocate(data, size);
	}
	
	template < typename T, typename A >
	void array_base<T,A>::_do_alloc_self(size_t n){
		MTL_ASSERT(!_first && !_last && !_capacity);
		if ( n > 0 ) {
			_first = _last = _do_alloc(n);
			_capacity = _first + n;
		}
	}
	
	template < typename T, typename A >
	void array_base<T,A>::_do_dealloc_self(){
		if ( _first ) {
			_do_dealloc(_first, _capacity - _first);
			_first = _last = _capacity = NULL;
		}
	}
	
	template < typename T, typename A >
	void array_base<T,A>::_do_realloc_self(size_t new_capacity){
		const size_t self_size = _last - _first;
		MTL_ASSERT(new_capacity >= self_size);
		if ( new_capacity > 0 ) {
			T* new_first = _do_alloc(new_capacity);
			try {
				internal::fill_range(new_first, _first, _last);
			} catch(...) {
				_do_dealloc(new_first, new_capacity);
				throw;
			}
			internal::destroy_range(_first, _last);
			_do_dealloc_self();
			_first    = new_first;
			_last     = new_first + self_size;
			_capacity = new_first + new_capacity;
		} else {
			_do_dealloc_self();
		}
	}
	
	template < typename T, typename A >
	size_t array_base<T,A>::_next_capacity(size_t minimum){
		const size_t grow_size = 2 * (_capacity - _first);
		return grow_size >= minimum ? grow_size : minimum;
	}
	
	// -------------------------------------------------------------------------
	// 
	// array
	// 
	// -------------------------------------------------------------------------
	
	template < typename T, typename A = allocator >
	class array : public array_base<T,A>{
	public:
		typedef T                                     value_type;
		typedef T*                                    pointer;
		typedef const T*                              const_pointer;
		typedef T&                                    reference;
		typedef const T&                              const_reference;
		typedef A                                     allocator_type;
		typedef array<T,A>                            self_type;
		typedef array_base<T,A>                       base_type;
	private:
		using base_type::_first;
		using base_type::_last;
		using base_type::_capacity;
		using base_type::_allocator;
	private:
		using base_type::_do_alloc;
		using base_type::_do_dealloc;
		using base_type::_do_alloc_self;
		using base_type::_do_dealloc_self;
		using base_type::_do_realloc_self;
		using base_type::_next_capacity;
	private:
		void _insert(size_t index, size_t self_size, const T& value);
		void _insert(size_t index, size_t self_size, size_t n, const T& value);
		void _insert(size_t index, size_t self_size, const T* first, const T* last);
	public:
		explicit array(const A& allocator = A());
		explicit array(size_t n, const T& value = T(), const A& allocator = A());
		array(const T* first, const T* last, const A& allocator = A());
		array(const array& other);
		array(const array& other, const A& allocator);
		array& operator=(const array& other);
		~array();
		
		T*       data          ();
		const T* data          () const;
		const T* cdata         () const;
		
		T&       at            ( size_t index );
		const T& at            ( size_t index ) const;
		
		T&       operator[]    ( size_t index );
		const T& operator[]    ( size_t index ) const;
		
		void     pop_back      ();
		void     push_back     ( const T& value );
		
		void     erase         ( size_t index );
		void     erase         ( size_t first, size_t last );
		
		void     insert        ( size_t index, const T& value );
		void     insert        ( size_t index, size_t n, const T& value );
		void     insert        ( size_t index, const T* first, const T* last);
		
		void     assign        ( size_t n, const T& value );
		void     assign        ( const T* first, const T* last );
		
		T&       back          ();
		const T& back          () const;
		
		T&       front         ();
		const T& front         () const;
		
		size_t   size          () const;
		size_t   capacity      () const;
		bool     empty         () const;
		
		void     clear         ();
		void     shrink        ();
		void     reset         ();
		void     swap          ( array& other );
		void     reserve       ( size_t new_capacity );
		void     resize        ( size_t new_size );
		void     resize        ( size_t new_size, const T& value );
		
		A&       get_allocator ();
		const A& get_allocator () const;
	};
	
	// -------------------------------------------------------------------------
	// 
	// array operators
	// 
	// -------------------------------------------------------------------------
	
	template < typename T, typename A >
	inline bool operator==(const array<T,A>& left, const array<T,A>& right){
		const size_t left_size = left.size();
		return
			left_size == right.size() &&
			mtl::equal(left.data(), left.data() + left_size, right.data());
	}
	
	template < typename T, typename A >
	inline bool operator!=(const array<T,A>& left, const array<T,A>& right){
		return !(left == right);
	}
	
	template < typename T, typename A >
	inline bool operator<(const array<T,A>& left, const array<T,A>& right){
		return mtl::lexicographical_compare(
			 left.data(),  left.data() +  left.size(),
			right.data(), right.data() + right.size());
	}
	
	template < typename T, typename A >
	inline bool operator>(const array<T,A>& left, const array<T,A>& right){
		return right < left;
	}
	
	template < typename T, typename A >
	inline bool operator<=(const array<T,A>& left, const array<T,A>& right){
		return !(right < left);
	}
	
	template < typename T, typename A >
	inline bool operator>=(const array<T,A>& left, const array<T,A>& right){
		return !(left < right);
	}
	
	// -------------------------------------------------------------------------
	// 
	// array impl
	// 
	// -------------------------------------------------------------------------
	
	template < typename T, typename A >
	void array<T,A>::_insert(
		size_t index, size_t self_size, const T& value)
	{
		MTL_ASSERT(self_size == size() && index <= self_size);
		if ( _last < _capacity ) {
			if ( index == self_size ) {
				internal::construct(_last, value);
				++_last;
			} else {
				T* pos = _first + index;
				const T value_copy = value;
				internal::construct(_last, *(_last - 1));
				++_last;
				internal::copy_range_backward(_last - 1, pos, _last - 2);
				*pos = value_copy;
			}
		} else {
			const size_t new_size = _next_capacity(self_size + 1);
			T* new_first = _do_alloc(new_size);
			T* new_last  = new_first;
			try {
				T* pos = _first + index;
				new_last = internal::fill_range(new_first, _first, pos);
				internal::construct(new_last, value);
				++new_last;
				new_last = internal::fill_range(new_last, pos, _last);
			} catch(...) {
				internal::destroy_range(new_first, new_last);
				_do_dealloc(new_first, new_size);
				throw;
			}
			reset();
			_first    = new_first;
			_last     = new_last;
			_capacity = new_first + new_size;
		}
	}
	
	template < typename T, typename A >
	void array<T,A>::_insert(
		size_t index, size_t self_size, size_t n, const T& value)
	{
		MTL_ASSERT(self_size == size() && index <= self_size && n > 0);
		const size_t free_capacity = _capacity - _last;
		if ( n <= free_capacity ) {
			T* old_last        = _last;
			T* position        = _first + index;
			const T value_copy = value;
			const size_t extra = _last - position;
			if ( n < extra ) {
				_last = internal::fill_range(_last, _last - n, _last);
				internal::copy_range_backward(old_last, position, old_last - n);
				internal::copy_range_n(position, n, value_copy);
			} else {
				_last = internal::fill_range_n(_last, n - extra, value_copy);
				_last = internal::fill_range(_last, position, old_last);
				internal::copy_range_n(position, extra, value_copy);
			}
		} else {
			const size_t new_size = _next_capacity(self_size + n);
			T* position  = _first + index;
			T* new_first = _do_alloc(new_size);
			T* new_last  = new_first;
			try {
				new_last = internal::fill_range(new_first, _first, position);
				new_last = internal::fill_range_n(new_last, n, value);
				new_last = internal::fill_range(new_last, position, _last);
			} catch(...) {
				internal::destroy_range(new_first, new_last);
				_do_dealloc(new_first, new_size);
				throw;
			}
			reset();
			_first    = new_first;
			_last     = new_last;
			_capacity = new_first + new_size;
		}
	}
	
	template < typename T, typename A >
	void array<T,A>::_insert(
		size_t index, size_t self_size, const T* first, const T* last)
	{
		MTL_ASSERT(self_size == size() && index <= self_size);
		MTL_ASSERT(first && last && first <= last);
		const size_t n = last - first;
		const size_t free_capacity = _capacity - _last;
		if ( n <= free_capacity ) {
			T* old_last        = _last;
			T* position        = _first + index;
			const size_t extra = _last - position;
			if ( n < extra ) {
				_last = internal::fill_range(_last, _last - n, _last);
				internal::copy_range_backward(old_last, position, old_last - n);
				internal::copy_range(position, first, last);
			} else {
				const T* tmp = first + extra;
				_last = internal::fill_range(_last, tmp, last);
				_last = internal::fill_range(_last, position, old_last);
				internal::copy_range_backward(position + extra, first, tmp);
			}
		} else {
			const size_t new_size = _next_capacity(self_size + n);
			T* position  = _first + index;
			T* new_first = _do_alloc(new_size);
			T* new_last  = new_first;
			try {
				new_last = internal::fill_range(new_first, _first, position);
				new_last = internal::fill_range(new_last, first, last);
				new_last = internal::fill_range(new_last, position, _last);
			} catch(...) {
				internal::destroy_range(new_first, new_last);
				_do_dealloc(new_first, new_size);
				throw;
			}
			reset();
			_first    = new_first;
			_last     = new_last;
			_capacity = new_first + new_size;
		}
	}
	
	template < typename T, typename A >
	array<T,A>::array(const A& allocator)
	: base_type(allocator){
	}
	
	template < typename T, typename A >
	array<T,A>::array(size_t n, const T& value, const A& allocator)
	: base_type(allocator){
		assign(n, value);
	}
	
	template < typename T, typename A >
	array<T,A>::array(const T* first, const T* last, const A& allocator)
	: base_type(allocator){
		assign(first, last);
	}
	
	template < typename T, typename A >
	array<T,A>::array(const array& other)
	: base_type(other.get_allocator()){
		assign(other._first, other._last);
	}
	
	template < typename T, typename A >
	array<T,A>::array(const array& other, const A& allocator)
	: base_type(allocator){
		assign(other._first, other._last);
	}
	
	template < typename T, typename A >
	array<T,A>& array<T,A>::operator=(const array& other){
		if ( this != &other ) {
			if ( get_allocator() != other.get_allocator() ) {
				reset();
				_allocator = other.get_allocator();
			}
			assign(other._first, other._last);
		}
		return *this;
	}
	
	template < typename T, typename A >
	array<T,A>::~array(){
		reset();
	}
	
	template < typename T, typename A >
	T* array<T,A>::data(){
		return _first;
	}
	
	template < typename T, typename A >
	const T* array<T,A>::data() const{
		return _first;
	}
	
	template < typename T, typename A >
	const T* array<T,A>::cdata() const{
		return _first;
	}
	
	template < typename T, typename A >
	T& array<T,A>::at(size_t index){
		if ( _first + index >= _last ) {
			throw std::out_of_range("array::_check_range");
		}
		return *(_first + index);
	}
	
	template < typename T, typename A >
	const T& array<T,A>::at(size_t index) const{
		if ( _first + index >= _last ) {
			throw std::out_of_range("array::_check_range");
		}
		return *(_first + index);
	}
	
	template < typename T, typename A >
	T& array<T,A>::operator[](size_t index){
		MTL_ASSERT(index < size());
		return *(_first + index);
	}
	
	template < typename T, typename A >
	const T& array<T,A>::operator[](size_t index) const{
		MTL_ASSERT(index < size());
		return *(_first + index);
	}
	
	template < typename T, typename A >
	void array<T,A>::pop_back(){
		MTL_ASSERT(!empty());
		internal::destroy(--_last);
	}
	
	template < typename T, typename A >
	void array<T,A>::push_back(const T& value){
		if ( _last < _capacity ) {
			internal::construct(_last, value);
			++_last;
		} else {
			const size_t self_size = size();
			_insert(self_size, self_size, value);
		}
	}
	
	template < typename T, typename A >
	void array<T,A>::erase(size_t index){
		MTL_ASSERT(index < size());
		internal::copy_range(_first + index, _first + index + 1, _last);
		internal::destroy(--_last);
	}
	
	template < typename T, typename A >
	void array<T,A>::erase(size_t first, size_t last){
		MTL_ASSERT(first < last && first < size() && last <= size());
		T* pos = internal::copy_range(_first + first, _first + last, _last);
		internal::destroy_range(pos, _last);
		_last = pos;
	}
	
	template < typename T, typename A >
	void array<T,A>::insert(size_t index, const T& value){
		const size_t self_size = size();
		MTL_ASSERT(index <= self_size);
		_insert(index, self_size, value);
	}
	
	template < typename T, typename A >
	void array<T,A>::insert(size_t index, size_t n, const T& value){
		const size_t self_size = size();
		MTL_ASSERT(index <= self_size && n > 0);
		_insert(index, self_size, n, value);
	}
	
	template < typename T, typename A >
	void array<T,A>::insert(size_t index, const T* first, const T* last){
		const size_t self_size = size();
		MTL_ASSERT(index <= self_size && first && last && first <= last);
		_insert(index, self_size, first, last);
	}
	
	template < typename T, typename A >
	void array<T,A>::assign(size_t n, const T& value){
		if ( n > capacity() ) {
			reset();
			_do_alloc_self(n);
			internal::fill_range_n(_first, n, value);
		} else if ( n > size() ) {
			const size_t sep = n - size();
			internal::copy_range_n(_first, n - sep, value);
			internal::fill_range_n(_last, sep, value);
		} else {
			internal::copy_range_n(_first, n, value);
			internal::destroy_range(_first + n, _last);
		}
		_last = _first + n;
	}
	
	template < typename T, typename A >
	void array<T,A>::assign(const T* first, const T* last){
		MTL_ASSERT(first <= last);
		const size_t n = last - first;
		if ( n > capacity() ) {
			reset();
			_do_alloc_self(n);
			internal::fill_range(_first, first, last);
		} else if ( n > size() ) {
			const T* sep = first + size();
			internal::copy_range(_first, first, sep);
			internal::fill_range(_last, sep, last);
		} else {
			internal::copy_range(_first, first, last);
			internal::destroy_range(_first + n, _last);
		}
		_last = _first + n;
	}
	
	template < typename T, typename A >
	T& array<T,A>::back(){
		MTL_ASSERT(!empty());
		return *(_last - 1);
	}
	
	template < typename T, typename A >
	const T& array<T,A>::back() const{
		MTL_ASSERT(!empty());
		return *(_last - 1);
	}
	
	template < typename T, typename A >
	T& array<T,A>::front(){
		MTL_ASSERT(!empty());
		return *_first;
	}
	
	template < typename T, typename A >
	const T& array<T,A>::front() const{
		MTL_ASSERT(!empty());
		return *_first;
	}
	
	template < typename T, typename A >
	size_t array<T,A>::size() const{
		return static_cast<size_t>(_last - _first);
	}
	
	template < typename T, typename A >
	size_t array<T,A>::capacity() const{
		return static_cast<size_t>(_capacity - _first);
	}
	
	template < typename T, typename A >
	bool array<T,A>::empty() const{
		return _first == _last;
	}
	
	template < typename T, typename A >
	void array<T,A>::clear(){
		internal::destroy_range(_first, _last);
		_last = _first;
	}
	
	template < typename T, typename A >
	void array<T,A>::shrink(){
		const size_t self_size = size();
		if ( capacity() > self_size ) {
			_do_realloc_self(self_size);
		}
	}
	
	template < typename T, typename A >
	void array<T,A>::reset(){
		clear();
		shrink();
	}
	
	template < typename T, typename A >
	void array<T,A>::swap(array& other){
		internal::swap_values(_first,     other._first);
		internal::swap_values(_last,      other._last);
		internal::swap_values(_capacity,  other._capacity);
		internal::swap_values(_allocator, other._allocator);
	}
	
	template < typename T, typename A >
	void array<T,A>::reserve(size_t new_capacity){
		if ( new_capacity > capacity() ) {
			_do_realloc_self(new_capacity);
		}
	}
	
	template < typename T, typename A >
	void array<T,A>::resize(size_t new_size){
		const size_t old_size = size();
		if ( new_size != old_size ) {
			if ( new_size > old_size ) {
				insert(old_size, new_size - old_size, T());
			} else {
				erase(new_size, old_size);
			}
		}
	}
	
	template < typename T, typename A >
	void array<T,A>::resize(size_t new_size, const T& value){
		const size_t old_size = size();
		if ( new_size != old_size ) {
			if ( new_size > old_size ) {
				insert(old_size, new_size - old_size, value);
			} else {
				erase(new_size, old_size);
			}
		}
	}
	
	template < typename T, typename A >
	A& array<T,A>::get_allocator(){
		return _allocator;
	}
	
	template < typename T, typename A >
	const A& array<T,A>::get_allocator() const{
		return _allocator;
	}
} // namespace mtl
