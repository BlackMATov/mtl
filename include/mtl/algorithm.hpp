#pragma once

#include "iterator.hpp"

namespace mtl
{
	// --------------------------------
	// lexicographical_compare
	// --------------------------------
	
	template < typename Iterator1, typename Iterator2 >
	inline bool lexicographical_compare(
		Iterator1 first1, Iterator1 last1, Iterator2 first2, Iterator2 last2)
	{
		for (; first1 != last1 && first2 != last2; ++first1, ++first2) {
			if ( *first1 < *first2 ) {
				return true;
			}
			if ( *first2 < *first1 ) {
				return false;
			}
		}
		return first1 == last1 && first2 == last2;
	}
	
	// --------------------------------
	// identical
	// --------------------------------
	
	template < typename Iterator1, typename Iterator2 >
	inline bool identical(
		Iterator1 first1, Iterator1 last1, Iterator2 first2, Iterator2 last2)
	{
		while ( first1 != last1 && first2 != last2 && *first1 == *first2 ) {
			++first1;
			++first2;
		}
		return first1 == last1 && first2 == last2;
	}
	
	// --------------------------------
	// equal
	// --------------------------------
	
	template < typename Iterator1, typename Iterator2 >
	inline bool equal(Iterator1 first1, Iterator1 last1, Iterator2 first2){
		for (; first1 != last1; ++first1, ++first2) {
			if ( !(*first1 == *first2) ) {
				return false;
			}
		}
		return true;
	}
} // namespace mtl
