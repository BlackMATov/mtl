#pragma once

#include <mtl/internal/base.hpp>

/// \todo add alignment
/// \todo add tag or name
/// \todo add allocation hooks

namespace mtl
{
	// -------------------------------------------------------------------------
	// 
	// allocator
	// 
	// -------------------------------------------------------------------------
	
	class allocator{
	public:
		allocator();
		allocator(const allocator& other);
		allocator& operator=(const allocator& other);
		~allocator();
		void* allocate(size_t size);
		void deallocate(void* ptr, size_t size);
	};
	
	// -------------------------------------------------------------------------
	// 
	// allocator impl
	// 
	// -------------------------------------------------------------------------
	
	inline allocator::allocator(){
	}
	
	inline allocator::allocator(const allocator& other){
		(void)other;
	}
	
	inline allocator& allocator::operator=(const allocator& other){
		(void)other;
		return *this;
	}
	
	inline allocator::~allocator(){
	}
	
	inline void* allocator::allocate(size_t size){
		void* ret = size > 0 ? MTL_malloc(size) : NULL;
		if ( !ret ) {
			throw std::bad_alloc();
		}
		return ret;
	}
	
	inline void allocator::deallocate(void* ptr, size_t size){
		(void)size;
		MTL_free(ptr);
	}
	
	inline bool operator==(const allocator& lhs, const allocator& rhs){
		(void)lhs;
		(void)rhs;
		return true;
	}
	
	inline bool operator!=(const allocator& lhs, const allocator& rhs){
		(void)lhs;
		(void)rhs;
		return false;
	}
} // namespace mtl
