#pragma once

#include <mtl/internal/base.hpp>

namespace mtl { namespace internal
{
	// --------------------------------
	// integral_constant
	// --------------------------------
	
	template < typename T, T v >
	struct integral_constant{
		static const T value = v;
		typedef T value_type;
		typedef integral_constant<T, v> type;
	};
	typedef integral_constant<bool, true>  true_type;
	typedef integral_constant<bool, false> false_type;
	
	// --------------------------------
	// is_void
	// --------------------------------
	
	template < typename T >
	struct is_void : false_type{};
	
	template <> struct is_void<void>                : true_type{};
	template <> struct is_void<const void>          : true_type{};
	template <> struct is_void<volatile void>       : true_type{};
	template <> struct is_void<const volatile void> : true_type{};
	
	// --------------------------------
	// is_integral
	// --------------------------------
	
	template < typename T >
	struct is_integral : false_type{};
	
	#define MTL_TMP_DECLARE_INTEGRAL(T)\
		template <> struct is_integral<T>                : true_type{};\
		template <> struct is_integral<T const>          : true_type{};\
		template <> struct is_integral<T volatile>       : true_type{};\
		template <> struct is_integral<T const volatile> : true_type{};
		
		MTL_TMP_DECLARE_INTEGRAL(bool)
		MTL_TMP_DECLARE_INTEGRAL(char)
		MTL_TMP_DECLARE_INTEGRAL(wchar_t)
		
		MTL_TMP_DECLARE_INTEGRAL(unsigned char)
		MTL_TMP_DECLARE_INTEGRAL(unsigned short)
		MTL_TMP_DECLARE_INTEGRAL(unsigned int)
		MTL_TMP_DECLARE_INTEGRAL(unsigned long)
		MTL_TMP_DECLARE_INTEGRAL(unsigned long long)
		
		MTL_TMP_DECLARE_INTEGRAL(signed char)
		MTL_TMP_DECLARE_INTEGRAL(signed short)
		MTL_TMP_DECLARE_INTEGRAL(signed int)
		MTL_TMP_DECLARE_INTEGRAL(signed long)
		MTL_TMP_DECLARE_INTEGRAL(signed long long)
	#undef MTL_TMP_DECLARE_INTEGRAL
	
	// --------------------------------
	// is_floating_point
	// --------------------------------
	
	template < typename T >
	struct is_floating_point : false_type{};
	
	#define MTL_TMP_DECLARE_FLOATING_POINT(T)\
		template <> struct is_floating_point<T>                : true_type{};\
		template <> struct is_floating_point<T const>          : true_type{};\
		template <> struct is_floating_point<T volatile>       : true_type{};\
		template <> struct is_floating_point<T const volatile> : true_type{};
		
		MTL_TMP_DECLARE_FLOATING_POINT(float)
		MTL_TMP_DECLARE_FLOATING_POINT(double)
		MTL_TMP_DECLARE_FLOATING_POINT(long double)
	#undef MTL_TMP_DECLARE_FLOATING_POINT
	
	// --------------------------------
	// is_pod
	// --------------------------------
	
	template < typename T >
	struct is_pod : integral_constant<bool,
		is_void<T>::value || is_integral<T>::value || is_floating_point<T>::value>{};
	
	template < typename T > struct is_pod<T*>                : true_type{};
	template < typename T > struct is_pod<const T*>          : true_type{};
	template < typename T > struct is_pod<volatile T*>       : true_type{};
	template < typename T > struct is_pod<const volatile T*> : true_type{};
}} // namespace mtl::internal

// ----------------------------------------------------------------------------
//
// External defines
//
// ----------------------------------------------------------------------------

#define MTL_EXTERNAL_DECLARE_POD(T)\
	namespace mtl { namespace internal { template <> struct is_pod<T> : true_type{}; }}
	
#define MTL_EXTERNAL_DECLARE_POD_WITH_CV(T)\
	MTL_EXTERNAL_DECLARE_POD(T)\
	MTL_EXTERNAL_DECLARE_POD(const T)\
	MTL_EXTERNAL_DECLARE_POD(volatile T)\
	MTL_EXTERNAL_DECLARE_POD(const volatile T)
