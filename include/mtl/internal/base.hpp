#pragma once

#include <cmath>
#include <ctime>
#include <cstdio>
#include <cstddef>
#include <cstdlib>
#include <cassert>
#include <cstring>
#include <cassert>

#include <new>
#include <stdexcept>

// ------------------------------------
// C++11
// ------------------------------------

#if __cplusplus > 199711L
#	define MTL_HAS_CPP11
#endif

// ------------------------------------
// C
// ------------------------------------

#define MTL_memcpy  ::memcpy
#define MTL_memmove ::memmove

#define MTL_malloc  ::malloc
#define MTL_free    ::free

// ------------------------------------
// assert
// ------------------------------------

#ifndef MTL_PREPROCESSOR_JOIN
#	define MTL_PREPROCESSOR_JOIN(a,b)  MTL_PREPROCESSOR_JOIN1(a,b)
#	define MTL_PREPROCESSOR_JOIN1(a,b) MTL_PREPROCESSOR_JOIN2(a,b)
#	define MTL_PREPROCESSOR_JOIN2(a,b) a##b
#endif

#define MTL_STATIC_ASSERT(expr)\
	enum { MTL_PREPROCESSOR_JOIN(mtl_static_assert_, __LINE__) = 1 / ((!!(expr)) ? 1 : 0) }

#define MTL_ASSERT(expr)          assert((expr))
#define MTL_ASSERT_MSG(expr, msg) assert((expr) && (msg))
