#pragma once

#include "traits.hpp"

namespace mtl { namespace internal
{
	// ----------------------------
	// construct
	// ----------------------------
	
	template < typename T, typename V >
	inline void construct_impl(T* ptr, const V& value, false_type)
	{
		::new(static_cast<void*>(ptr)) T(value);
	}
	
	template < typename T, typename V >
	inline void construct_impl(T* ptr, const V& value, true_type)
	{
		*ptr = value;
	}
	
	template < typename T, typename V >
	inline void construct(T* ptr, const V& value)
	{
		construct_impl(ptr, value, is_pod<T>());
	}
	
	// ----------------------------
	// destroy
	// ----------------------------
	
	template < typename T >
	inline void destroy_impl(T* ptr, false_type)
	{
		(void)ptr;
		ptr->~T();
	}
	
	template < typename T >
	inline void destroy_impl(T* ptr, true_type)
	{
		(void)ptr;
	}
	
	template < typename T >
	inline void destroy(T* ptr)
	{
		destroy_impl(ptr, is_pod<T>());
	}
	
	// ----------------------------
	// destroy_range
	// ----------------------------
	
	template < typename T >
	inline void destroy_range_impl(T* first, const T* last, false_type)
	{
		MTL_ASSERT(first <= last);
		for(; first != last; ++first) {
			destroy(first);
		}
	}
	
	template < typename T >
	inline void destroy_range_impl(T* first, const T* last, true_type)
	{
		(void)first;
		(void)last;
	}
	
	template < typename T >
	inline void destroy_range(T* first, const T* last)
	{
		destroy_range_impl(first, last, is_pod<T>());
	}
	
	// ----------------------------
	// fill_range_n value
	// ----------------------------
	
	template < typename T >
	inline T* fill_range_n_impl(
		T* first, size_t n, const T& value, false_type)
	{
		T* cur = first;
		try {
			for(; n > 0; --n, ++cur) {
				construct(cur, value);
			}
			return cur;
		} catch(...) {
			destroy_range(first, cur);
			throw;
		}
	}
	
	template < typename T >
	inline T* fill_range_n_impl(
		T* first, size_t n, const T& value, true_type)
	{
		for(; n > 0; --n, ++first) {
			*first = value;
		}
		return first;
	}
	
	template < typename T >
	inline T* fill_range_n(T* first, size_t n, const T& value)
	{
		return fill_range_n_impl(first, n, value, is_pod<T>());
	}
	
	// ----------------------------
	// fill_range range
	// ----------------------------
	
	template < typename T >
	inline T* fill_range_impl(
		T* dest, const T* first, const T* last, false_type)
	{
		T* cur = dest;
		try {
			for(; first != last; ++cur, ++first) {
				construct(cur, *first);
			}
			return cur;
		} catch(...) {
			destroy_range(dest, cur);
			throw;
		}
	}
	
	template < typename T >
	inline T* fill_range_impl(
		T* dest, const T* first, const T* last, true_type)
	{
		const size_t n  = last - first;
		MTL_memmove(dest, first, n * sizeof(T));
		return dest + n;
	}
	
	template < typename T >
	inline T* fill_range(T* dest, const T* first, const T* last)
	{
		MTL_ASSERT(first <= last);
		return fill_range_impl(dest, first, last, is_pod<T>());
	}
	
	// ----------------------------
	// copy_range_n value
	// ----------------------------
	
	template < typename T >
	inline T* copy_range_n(T* first, size_t n, const T& value)
	{
		for(; n > 0; --n, ++first) {
			*first = value;
		}
		return first;
	}
	
	// ----------------------------
	// copy_range range
	// ----------------------------
	
	template < typename T >
	inline T* copy_range_impl(
		T* dest, const T* first, const T* last, false_type)
	{
		for(; first != last; ++dest, ++first) {
			*dest = *first;
		}
		return dest;
	}
	
	template < typename T >
	inline T* copy_range_impl(
		T* dest, const T* first, const T* last, true_type)
	{
		const size_t n = last - first;
		MTL_memmove(dest, first, n * sizeof(T));
		return dest + n;
	}
	
	template < typename T >
	inline T* copy_range(T* dest, const T* first, const T* last)
	{
		MTL_ASSERT(first <= last);
		return copy_range_impl(dest, first, last, is_pod<T>());
	}
	
	// ----------------------------
	// copy_range_backward
	// ----------------------------
	
	template < typename T >
	inline void copy_range_backward_impl(
		T* dest, const T* first, const T* last, false_type)
	{
		for(size_t n = last - first; n > 0; --n) {
			*--dest = *--last;
		}
	}
	
	template < typename T >
	inline void copy_range_backward_impl(
		T* dest, const T* first, const T* last, true_type)
	{
		const size_t n = last - first;
		MTL_memmove(dest - n, first, n * sizeof(T));
	}
	
	template < typename T >
	inline void copy_range_backward(T* dest, const T* first, const T* last)
	{
		MTL_ASSERT(first <= last);
		copy_range_backward_impl(dest, first, last, is_pod<T>());
	}
	
	// ----------------------------
	// swap
	// ----------------------------
	
	template < typename T >
	inline void swap_values(T& a, T& b)
	{
		T temp(a);
		a = b;
		b = temp;
	}
}} // namespace mtl::internal
