#pragma once

#include <mtl/internal/base.hpp>

namespace mtl { namespace _ilist
{
	struct default_tag{};
	
	struct null_disposer{
		template < typename T >
		void operator()(T){}
	};
	
	template < typename TAG >
	class algo;
	
	template < typename TAG >
	class hook{
	public:
		typedef hook<TAG>*       node_ptr;
		typedef const hook<TAG>* const_node_ptr;
	private:
		node_ptr _prev;
		node_ptr _next;
		friend class algo<TAG>;
	public:
		hook(node_ptr prev = NULL, node_ptr next = NULL)
		: _prev(prev), _next(next){
		}
		hook(const hook<TAG>&){
		}
		hook& operator=(const hook<TAG>&){
			return *this;
		}
		~hook(){
		}
	};
	
	template < typename TAG >
	class algo{
	public:
		typedef hook<TAG>                          node_type;
		typedef typename hook<TAG>::node_ptr       node_ptr;
		typedef typename hook<TAG>::const_node_ptr const_node_ptr;
	public:
		static void set_prev(node_ptr self, node_ptr prev){
			self->_prev = prev;
		}
		static void set_next(node_ptr self, node_ptr next){
			self->_next = next;
		}
		static node_ptr get_prev(node_ptr self){
			return self->_prev;
		}
		static node_ptr get_next(node_ptr self){
			return self->_next;
		}
		static const_node_ptr get_prev(const_node_ptr self){
			return self->_prev;
		}
		static const_node_ptr get_next(const_node_ptr self){
			return self->_next;
		}
		static void link(node_ptr self, node_ptr next){
			node_ptr prev = get_prev(next);
			set_prev(self, prev);
			set_next(self, next);
			set_prev(next, self);
			set_next(prev, self);
		}
		static void unlink(node_ptr self){
			node_ptr next = get_next(self);
			node_ptr prev = get_prev(self);
			set_next(prev, next);
			set_prev(next, prev);
			set_prev(self, NULL);
			set_next(self, NULL);
		}
		static bool is_linked(const_node_ptr self){
			const_node_ptr next = get_next(self);
			return !!next;
		}
		static void left_link(node_ptr self, node_ptr prev){
			set_prev(self, prev);
			set_next(prev, self);
		}
		
		static void right_link(node_ptr self, node_ptr next){
			set_next(self, next);
			set_prev(next, self);
		}
		static void transfer(node_ptr p, node_ptr f, node_ptr l){
			if ( f != l ) {
				node_ptr prev_p = get_prev(p);
				node_ptr prev_f = get_prev(f);
				node_ptr prev_l = get_prev(l);
				right_link(prev_l, p);
				right_link(prev_f, l);
				right_link(prev_p, f);
			}
		}
		static void transfer(node_ptr p, node_ptr e){
			node_ptr n = get_next(e);
			if ( n != p && e != p ) {
				node_ptr prev_p = get_prev(p);
				node_ptr prev_e = get_prev(e);
				right_link(prev_p, e);
				right_link(e, p);
				left_link(n, prev_e);
			}
		}
		static void swap_prev(node_ptr left, node_ptr right){
			node_ptr tmp = get_prev(left);
			set_prev(left, get_prev(right));
			set_prev(right, tmp);
		}
		static void swap_next(node_ptr left, node_ptr right){
			node_ptr tmp = get_next(left);
			set_next(left, get_next(right));
			set_next(right, tmp);
		}
		static void swap_nodes(node_ptr left, node_ptr right){
			if ( left != right ) {
				node_ptr next_left  = get_next(left);
				node_ptr prev_left  = get_prev(left);
				node_ptr next_right = get_next(right);
				node_ptr prev_right = get_prev(right);
				swap_prev(next_left, next_right);
				swap_next(prev_left, prev_right);
				swap_prev(left, right);
				swap_next(left, right);
			}
		}
	};
}} // namespace mtl::_ilist
