#pragma once

#include <mtl/internal/_ilist.hpp>
#include <mtl/iterator.hpp>
#include <mtl/algorithm.hpp>

namespace mtl
{
	// -------------------------------------------------------------------------
	// 
	// helpers
	// 
	// -------------------------------------------------------------------------
	
	template < typename TAG >
	struct ilist_hook{
		typedef _ilist::hook<TAG> type;
	};
	typedef _ilist::default_tag   ilist_default_tag;
	typedef _ilist::null_disposer ilist_null_disposer;
	
	// -------------------------------------------------------------------------
	// 
	// ilist_iterator
	// 
	// -------------------------------------------------------------------------
	
	template < typename TAG, typename T, typename TP, typename TR >
	class ilist_iterator : public iterator<T,TP,TR>{
	public:
		typedef _ilist::algo<TAG>             algo;
		typedef typename algo::node_ptr       node_ptr;
		typedef typename algo::const_node_ptr const_node_ptr;
	private:
		node_ptr _node;
	public:
		ilist_iterator();
		ilist_iterator(const_node_ptr node);
		ilist_iterator(const ilist_iterator<TAG,T,T*,T&>& other);
		node_ptr        node      ();
		const_node_ptr  node      () const;
		ilist_iterator& operator++();
		ilist_iterator  operator++(int);
		ilist_iterator& operator--();
		ilist_iterator  operator--(int);
		TR              operator* () const;
		TP              operator->() const;
	};
	
	// -------------------------------------------------------------------------
	// 
	// ilist_iterator impl
	// 
	// -------------------------------------------------------------------------
	
	template < typename TAG, typename T, typename TP, typename TR >
	ilist_iterator<TAG,T,TP,TR>::ilist_iterator()
	: _node(NULL){
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	ilist_iterator<TAG,T,TP,TR>::ilist_iterator(const_node_ptr node)
	: _node(const_cast<node_ptr>(node)){
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	ilist_iterator<TAG,T,TP,TR>::ilist_iterator(
		const ilist_iterator<TAG,T,T*,T&>& other)
	: _node(const_cast<node_ptr>(other.node())){
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	typename ilist_iterator<TAG,T,TP,TR>::node_ptr
		ilist_iterator<TAG,T,TP,TR>::node()
	{
		return _node;
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	typename ilist_iterator<TAG,T,TP,TR>::const_node_ptr
		ilist_iterator<TAG,T,TP,TR>::node() const
	{
		return _node;
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	ilist_iterator<TAG,T,TP,TR>& ilist_iterator<TAG,T,TP,TR>::operator++(){
		_node = algo::get_next(_node);
		return *this;
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	ilist_iterator<TAG,T,TP,TR> ilist_iterator<TAG,T,TP,TR>::operator++(int){
		ilist_iterator tmp(*this);
		_node = algo::get_next(_node);
		return tmp;
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	ilist_iterator<TAG,T,TP,TR>& ilist_iterator<TAG,T,TP,TR>::operator--(){
		_node = algo::get_prev(_node);
		return *this;
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	ilist_iterator<TAG,T,TP,TR> ilist_iterator<TAG,T,TP,TR>::operator--(int){
		ilist_iterator tmp(*this);
		_node = algo::get_prev(_node);
		return tmp;
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	TR ilist_iterator<TAG,T,TP,TR>::operator*() const{
		MTL_ASSERT(_node);
		return static_cast<TR>(*_node);
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	TP ilist_iterator<TAG,T,TP,TR>::operator->() const{
		MTL_ASSERT(_node);
		return static_cast<TP>(_node);
	}
	
	// -------------------------------------------------------------------------
	// 
	// ilist_iterator operators
	// 
	// -------------------------------------------------------------------------
	
	template < typename TAG, typename T, typename TP, typename TR >
	bool operator==(
		const ilist_iterator<TAG,T,TP,TR>& lhs,
		const ilist_iterator<TAG,T,TP,TR>& rhs)
	{
		return lhs.node() == rhs.node();
	}
	
	template < typename TAG, typename T, typename TP, typename TR >
	bool operator!=(
		const ilist_iterator<TAG,T,TP,TR>& lhs,
		const ilist_iterator<TAG,T,TP,TR>& rhs)
	{
		return lhs.node() != rhs.node();
	}
	
	template
	<
		typename TAG, typename T,
		typename LTP, typename LTR,
		typename RTP, typename RTR
	>
	bool operator==(
		const ilist_iterator<TAG,T,LTP,LTR>& lhs,
		const ilist_iterator<TAG,T,RTP,RTR>& rhs)
	{
		return lhs.node() == rhs.node();
	}
	
	template
	<
		typename TAG, typename T,
		typename LTP, typename LTR,
		typename RTP, typename RTR
	>
	bool operator!=(
		const ilist_iterator<TAG,T,LTP,LTR>& lhs,
		const ilist_iterator<TAG,T,RTP,RTR>& rhs)
	{
		return lhs.node() != rhs.node();
	}
	
	// -------------------------------------------------------------------------
	// 
	// ilist
	// 
	// -------------------------------------------------------------------------
	
	template < typename T, typename TAG = ilist_default_tag >
	class ilist{
	public:
		typedef T                                       value_type;
		typedef T*                                      pointer;
		typedef const T*                                const_pointer;
		typedef T&                                      reference;
		typedef const T&                                const_reference;
		typedef ilist_iterator<TAG,T,T*,T&>             iterator;
		typedef ilist_iterator<TAG,T,const T*,const T&> const_iterator;
		typedef mtl::reverse_iterator<iterator>         reverse_iterator;
		typedef mtl::reverse_iterator<const_iterator>   const_reverse_iterator;
	private:
		ilist(const ilist&);
		ilist& operator=(const ilist&);
	private:
		typedef _ilist::algo<TAG>                       algo;
		typedef typename algo::node_type                node_type;
		typedef typename algo::node_ptr                 node_ptr;
		typedef typename algo::const_node_ptr           const_node_ptr;
		node_type _root;
	public:
		ilist();
		template < typename TIt >
		ilist(TIt first, TIt last);
		~ilist();
		
		iterator               begin                 ();
		const_iterator         begin                 () const;
		const_iterator         cbegin                () const;
		
		iterator               end                   ();
		const_iterator         end                   () const;
		const_iterator         cend                  () const;
		
		reverse_iterator       rbegin                ();
		const_reverse_iterator rbegin                () const;
		const_reverse_iterator crbegin               () const;
		
		reverse_iterator       rend                  ();
		const_reverse_iterator rend                  () const;
		const_reverse_iterator crend                 () const;
		
		T&                     back                  ();
		const T&               back                  () const;
		
		T&                     front                 ();
		const T&               front                 () const;
		
		size_t                 size                  () const;
		bool                   empty                 () const;
		void                   clear                 ();
		void                   swap                  (ilist& other);
		
		void                   pop_back              ();
		void                   pop_front             ();
		void                   push_back             (T& value);
		void                   push_front            (T& value);
		
		iterator               erase                 (const_iterator pos);
		iterator               erase                 (const_iterator first, const_iterator last);
		
		iterator               insert                (const_iterator pos, T& value);
		template < typename TIt >
		void                   insert                (const_iterator pos, TIt first, TIt last);
		template < typename TIt >
		void                   assign                (TIt first, TIt last);
		
		void                   splice                (const_iterator pos, ilist& other);
		void                   splice                (const_iterator pos, ilist& other, const_iterator elem);
		void                   splice                (const_iterator pos, ilist& other, const_iterator first, const_iterator last);
		
		template < typename Disposer >
		void                   pop_back_and_dispose  (Disposer disposer);
		template < typename Disposer >
		void                   pop_front_and_dispose (Disposer disposer);
		template < typename Disposer >
		void                   clear_and_dispose     (Disposer disposer);
		template < typename Disposer >
		iterator               erase_and_dispose     (const_iterator pos, Disposer disposer);
		template < typename Disposer >
		iterator               erase_and_dispose     (const_iterator first, const_iterator last, Disposer disposer);
		template < typename TIt, typename Disposer >
		void                   dispose_and_assign    (Disposer disposer, TIt first, TIt last);
		
		static iterator        iterator_to           (T& value);
		static const_iterator  iterator_to           (const T& value);
	};
	
	// -------------------------------------------------------------------------
	// 
	// ilist impl
	// 
	// -------------------------------------------------------------------------
	
	template < typename T, typename TAG >
	ilist<T,TAG>::ilist()
	: _root(&_root, &_root){
	}
	
	template < typename T, typename TAG >
	template < typename TIt >
	ilist<T,TAG>::ilist(TIt first, TIt last)
	: _root(&_root, &_root){
		insert(cend(), first, last);
	}
	
	template < typename T, typename TAG >
	ilist<T,TAG>::~ilist(){
		clear();
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::begin(){
		return iterator(algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_iterator ilist<T,TAG>::begin() const{
		return const_iterator(algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_iterator ilist<T,TAG>::cbegin() const{
		return const_iterator(algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::end(){
		return iterator(&_root);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_iterator ilist<T,TAG>::end() const{
		return const_iterator(&_root);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_iterator ilist<T,TAG>::cend() const{
		return const_iterator(&_root);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::reverse_iterator ilist<T,TAG>::rbegin(){
		return reverse_iterator(&_root);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_reverse_iterator ilist<T,TAG>::rbegin() const{
		return const_reverse_iterator(&_root);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_reverse_iterator ilist<T,TAG>::crbegin() const{
		return const_reverse_iterator(&_root);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::reverse_iterator ilist<T,TAG>::rend(){
		return reverse_iterator(algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_reverse_iterator ilist<T,TAG>::rend() const{
		return const_reverse_iterator(algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_reverse_iterator ilist<T,TAG>::crend() const{
		return const_reverse_iterator(algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	T& ilist<T,TAG>::back(){
		MTL_ASSERT(!empty());
		return static_cast<T&>(*algo::get_prev(&_root));
	}
	
	template < typename T, typename TAG >
	const T& ilist<T,TAG>::back() const{
		MTL_ASSERT(!empty());
		return static_cast<const T&>(*algo::get_prev(&_root));
	}
	
	template < typename T, typename TAG >
	T& ilist<T,TAG>::front(){
		MTL_ASSERT(!empty());
		return static_cast<T&>(*algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	const T& ilist<T,TAG>::front() const{
		MTL_ASSERT(!empty());
		return static_cast<const T&>(*algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	size_t ilist<T,TAG>::size() const{
		size_t n = 0;
		const node_type* node = algo::get_next(&_root);
		while ( node != &_root ) {
			++n;
			node = algo::get_next(node);
		}
		return n;
	}
	
	template < typename T, typename TAG >
	bool ilist<T,TAG>::empty() const{
		return &_root == algo::get_next(&_root);
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::clear(){
		clear_and_dispose(ilist_null_disposer());
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::swap(ilist& other){
		algo::swap_nodes(&_root, &other._root);
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::pop_back(){
		pop_back_and_dispose(ilist_null_disposer());
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::pop_front(){
		pop_front_and_dispose(ilist_null_disposer());
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::push_back(T& value){
		/// \todo CHECK value
		node_type& node = static_cast<node_type&>(value);
		MTL_ASSERT(!algo::is_linked(&node));
		algo::link(&node, &_root);
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::push_front(T& value){
		/// \todo CHECK value
		node_type& node = static_cast<node_type&>(value);
		MTL_ASSERT(!algo::is_linked(&node));
		algo::link(&node, algo::get_next(&_root));
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::erase(const_iterator pos){
		return erase_and_dispose(pos, ilist_null_disposer());
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::erase(
		const_iterator first, const_iterator last)
	{
		return erase_and_dispose(first, last, ilist_null_disposer());
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::insert(
		const_iterator pos, T& value)
	{
		/// \todo CHECK value
		node_type& node = static_cast<node_type&>(value);
		MTL_ASSERT(!algo::is_linked(&node));
		algo::link(&node, pos.node());
		return iterator(&node);
	}
	
	template < typename T, typename TAG >
	template < typename TIt >
	void ilist<T,TAG>::insert(const_iterator pos, TIt first, TIt last){
		while ( first != last ) {
			insert(pos, *first);
			++first;
		}
	}
	
	template < typename T, typename TAG >
	template < typename TIt >
	void ilist<T,TAG>::assign(TIt first, TIt last){
		dispose_and_assign(ilist_null_disposer(), first, last);
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::splice(const_iterator pos, ilist& other){
		algo::transfer(
			pos.node(), other.begin().node(), other.end().node());
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::splice(
		const_iterator pos, ilist& other, const_iterator elem)
	{
		(void)other;
		algo::transfer(pos.node(), elem.node());
	}
	
	template < typename T, typename TAG >
	void ilist<T,TAG>::splice(
		const_iterator pos, ilist& other,
		const_iterator first, const_iterator last)
	{
		(void)other;
		algo::transfer(pos.node(), first.node(), last.node());
	}
	
	template < typename T, typename TAG >
	template < typename Disposer >
	void ilist<T,TAG>::pop_back_and_dispose(Disposer disposer){
		MTL_ASSERT(!empty());
		node_ptr node = algo::get_prev(&_root);
		algo::unlink(node);
		disposer(static_cast<T*>(node));
	}
	
	template < typename T, typename TAG >
	template < typename Disposer >
	void ilist<T,TAG>::pop_front_and_dispose(Disposer disposer){
		MTL_ASSERT(!empty());
		node_ptr node = algo::get_next(&_root);
		algo::unlink(node);
		disposer(static_cast<T*>(node));
	}
	
	template < typename T, typename TAG >
	template < typename Disposer >
	void ilist<T,TAG>::clear_and_dispose(Disposer disposer){
		while ( !empty() ) {
			pop_back_and_dispose(disposer);
		}
	}
	
	template < typename T, typename TAG >
	template < typename Disposer >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::erase_and_dispose(
		const_iterator pos, Disposer disposer)
	{
		node_ptr node = pos.node();
		++pos;
		algo::unlink(node);
		disposer(static_cast<T*>(node));
		return iterator(pos.node());
	}
	
	template < typename T, typename TAG >
	template < typename Disposer >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::erase_and_dispose(
		const_iterator first, const_iterator last, Disposer disposer)
	{
		while ( first != last ) {
			first = erase_and_dispose(first, disposer);
		}
		return iterator(last.node());
	}
	
	template < typename T, typename TAG >
	template < typename TIt, typename Disposer >
	void ilist<T,TAG>::dispose_and_assign(
		Disposer disposer, TIt first, TIt last)
	{
		clear_and_dispose(disposer);
		insert(cend(), first, last);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::iterator ilist<T,TAG>::iterator_to(T& value){
		/// \todo CHECK value
		node_type& node = static_cast<node_type&>(value);
		MTL_ASSERT(algo::is_linked(&node));
		return iterator(&node);
	}
	
	template < typename T, typename TAG >
	typename ilist<T,TAG>::const_iterator ilist<T,TAG>::iterator_to(
		const T& value)
	{
		/// \todo CHECK value
		const node_type& node = static_cast<const node_type&>(value);
		MTL_ASSERT(algo::is_linked(&node));
		return const_iterator(&node);
	}
	
	// -------------------------------------------------------------------------
	//
	// ilist operators
	//
	// -------------------------------------------------------------------------
	
	template < typename T, typename TAG >
	inline bool operator==(const ilist<T,TAG>& left, const ilist<T,TAG>& right){
		return mtl::identical(
			left.begin(), left.end(), right.begin(), right.end());
	}
	
	template < typename T, typename TAG >
	inline bool operator!=(const ilist<T,TAG>& left, const ilist<T,TAG>& right){
		return !(left == right);
	}
	
	template < typename T, typename TAG >
	inline bool operator<(const ilist<T,TAG>& left, const ilist<T,TAG>& right){
		return mtl::lexicographical_compare(
			left.begin(), left.end(), right.begin(), right.end());
	}
	
	template < typename T, typename TAG >
	inline bool operator<=(const ilist<T,TAG>& left, const ilist<T,TAG>& right){
		return !(right < left);
	}
	
	template < typename T, typename TAG >
	inline bool operator>(const ilist<T,TAG>& left, const ilist<T,TAG>& right){
		return right < left;
	}
	
	template < typename T, typename TAG >
	inline bool operator>=(const ilist<T,TAG>& left, const ilist<T,TAG>& right){
		return !(left < right);
	}
} // namespace mtl
