QT             -= core
QT             -= gui
#QMAKE_CXXFLAGS += -std=c++11

# for lib
#TEMPLATE = lib
#CONFIG  += staticlib

# for test app
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

CONFIG(debug, debug|release) {
  DEFINES += _DEBUG
  DEFINES += NRELEASE
  MTL_TARGET_NAME = mtl_d
} else {
  DEFINES += NDEBUG
  DEFINES += _RELEASE
  MTL_TARGET_NAME = mtl
}

win32 {
  TARGET = ../../../../lib/mingw/$$MTL_TARGET_NAME
}
unix {
  TARGET = ../../../lib/mac/$$MTL_TARGET_NAME
}

INCLUDEPATH += \
../../../include

HEADERS += \
../../../include/mtl/*.hpp\
../../../include/mtl/internal/*.hpp\
../../../src/*.hpp

SOURCES += \
../../../src/*.*\
../../../UnitTest++/*.cpp\
../../../UnitTest++/Win32/*.cpp
