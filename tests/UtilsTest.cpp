
#include "../precomp.h"
#include "UtilsTest.h"

// ----------------------------------------------------------------------------
//
// AutoTimer
//
// ----------------------------------------------------------------------------

AutoTimer::AutoTimer(const char* name, clock_t last_time)
: _name(name), _clock_time(::clock()), _last_time(last_time)
{
	
}

AutoTimer::~AutoTimer()
{
	clock_t clock_time_2 = getTime();
	std::cout << _name.c_str() << ": " << clock_time_2;
	if ( _last_time && clock_time_2 > 0 )
		std::cout << " x" << (double)_last_time / (double)clock_time_2 << std::endl;
	else
		std::cout << std::endl;
}

clock_t AutoTimer::getTime() const
{
	return ::clock() - _clock_time;
}

// ----------------------------------------------------------------------------
//
// UtilsTest
//
// ----------------------------------------------------------------------------

bool UtilsTest::OnInit()
{
	Path::BindAlias("assets", Path::res_dir / "Sample_Tests");

	if ( !UtilsTest_Array() )
		return false;
	
	if ( !UtilsTest_Pool() )
		return false;
	
	if ( !UtilsTest_List() )
		return false;
		
	if ( !UtilsTest_Iterator() )
		return false;
	
	return true;
}

void UtilsTest::OnShutdown()
{

}

bool UtilsTest::OnUpdate()
{
	return true;
}

void UtilsTest::OnRender()
{
	theRenderer()->ClearScene(Color::cyan);
}
