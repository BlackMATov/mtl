
#include "../precomp.h"
#include "main.h"

#include "UtilsTest.h"

// ----------------------------------------------------------------------------
//
// Start point
//
// ----------------------------------------------------------------------------

void BMEMain()
{
#if BME_COMPILER == BME_COMPILER_MSVC && BME_MODE == BME_MODE_DEBUG
	ActivateMemoryControl();
#endif

	Application::Desc desc;
	desc.resolution = pnt2u(1024, 768);
	desc.isVsync = false;
	desc.isWidescreen = true;
	desc.isNoFocusSuspend = false;
	desc.isFullscreenCursorClip = false;
	BMEStart<MyGame>(desc);
}

// ----------------------------------------------------------------------------
//
// MyGame
//
// ----------------------------------------------------------------------------

MyGame::MyGame()
{
	_test = TestPtr(new UtilsTest());
}

MyGame::~MyGame()
{

}

bool MyGame::OnInit()
{
	Path path = Path::app_dir;
	if ( theFilesystem()->CreateDirectories(path) )
		theLogger()->setLogfile(path / "log.txt");

	theLogger()->setMinLevel(Logger::LEVEL_DEBUG);
	theLogger()->setOutputType(Logger::OUTPUT_ALL);
	return _test->OnInit();
}

void MyGame::OnShutdown()
{
	_test->OnShutdown();
}

bool MyGame::OnUpdate()
{
	if ( theKeyboard()->IsKeyDown(KEY_ESCAPE))
		return false;

	if ( theKeyboard()->IsKeyDown(KEY_F1) )
		ToggleFullscreen( !getDesc().isFullscreen );

	if ( theKeyboard()->IsKeyDown(KEY_F2) )
		ToggleWidescreen( !getDesc().isWidescreen );

	// set caption
	Str caption = BME_Str_Make(512, "Sample 'Tests'. FPS[%1%] MP[%2%]",
		getFPS(),
		theMouse() ? theMouse()->getPosition() : vec2f::zero);
	theSystem()->setWindowCaption(caption);
	
	return _test->OnUpdate();
}

void MyGame::OnRender()
{
	theRenderer()->BeginScene();
	_test->OnRender();
	theRenderer()->EndScene();
}
