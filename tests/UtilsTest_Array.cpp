
#include "../precomp.h"
#include "UtilsTest.h"

namespace utils_array_test
{
	bool Test0()
	{
		return true;
		
		// compile test
		
		typedef Array<size_t, 5> TArray;
		TArray arr;
		TArray arr2 = arr;
		arr = arr2;
		
		TArray::iterator               i01 = arr.begin();
		TArray::const_iterator         i02 = arr.begin();
		TArray::const_iterator         i03 = arr.cbegin();
		TArray::iterator               i04 = arr.end();
		TArray::const_iterator         i05 = arr.end();
		TArray::const_iterator         i06 = arr.cend();
		
		TArray::reverse_iterator       i07 = arr.rbegin();
		TArray::const_reverse_iterator i08 = arr.rbegin();
		TArray::const_reverse_iterator i09 = arr.crbegin();
		TArray::reverse_iterator       i10 = arr.rend();
		TArray::const_reverse_iterator i11 = arr.rend();
		TArray::const_reverse_iterator i12 = arr.crend();
		
		TArray::reference              r01 = arr.at(0);
		TArray::const_reference        r02 = arr.at(0);
		TArray::reference              r03 = arr[0];
		TArray::const_reference        r04 = arr[0];
		
		arr.pop_back();
		arr.push_back(0);
		
		TArray::reference              r05 = arr.back();
		TArray::const_reference        r06 = arr.back();
		TArray::reference              r07 = arr.front();
		TArray::const_reference        r08 = arr.front();
		
		arr.clear();
		TArray::size_type              s01 = arr.size();
		TArray::size_type              s02 = arr.capacity();
		bool                           b01 = arr.full();
		bool                           b02 = arr.empty();
		arr.resize(10);
		
		arr.sort();
		arr.stable_sort();
		bool                           b03 = arr.is_sorted();
		TArray::iterator               i13 = arr.push_sorted(0);
		
		TArray::iterator               i14 = arr.erase(arr.begin());
		TArray::iterator               i15 = arr.erase(arr.cbegin());
		arr.erase_unordered(arr.begin());
		arr.erase_unordered(arr.cbegin());
		
		TArray::iterator               i16 = arr.insert(arr.begin(),  0);
		TArray::iterator               i17 = arr.insert(arr.cbegin(), 0);
		arr.insert_unordered(arr.begin(), 0);
		arr.insert_unordered(arr.begin(), 0);
		
		TArray::iterator               i18 = arr.linear_find(0);
		TArray::const_iterator         i19 = arr.linear_find(0);
		TArray::iterator               i20 = arr.binary_find(0);
		TArray::const_iterator         i21 = arr.binary_find(0);
		
		arr.sort(cpp::less<size_t>());
		arr.stable_sort(cpp::less<size_t>());
		bool                           b04 = arr.is_sorted(cpp::less<size_t>());
		TArray::iterator               i22 = arr.push_sorted(0,cpp::less<size_t>());
		TArray::iterator               i23 = arr.binary_find(0,cpp::less<size_t>());
		TArray::const_iterator         i24 = arr.binary_find(0,cpp::less<size_t>());
		
		(void)i01; (void)i02; (void)i03; (void)i04; (void)i05;
		(void)i06; (void)i07; (void)i08; (void)i09; (void)i10;
		(void)i11; (void)i12; (void)i13; (void)i14; (void)i15;
		(void)i16; (void)i17; (void)i18; (void)i19; (void)i20;
		(void)i21; (void)i22; (void)i23; (void)i24;
		(void)r01; (void)r02; (void)r03; (void)r04; (void)r05;
		(void)r06; (void)r07; (void)r08;
		(void)s01; (void)s02;
		(void)b01; (void)b02; (void)b03; (void)b04;
		
		return true;
	}

	bool Test1()
	{
		typedef Array<size_t, 5> TArray;
		TArray arr1;
		
		{
			for ( size_t i = 0; i < arr1.capacity(); ++i )
			{
				if ( arr1.size() != i )
					return false;
				
				arr1.push_back(i);
				
				if ( arr1.size() != i+1 )
					return false;
			}
			
			if ( !arr1.full() )
				return false;
		}
		
		TArray arr2 = arr1;
		
		{
			size_t t = arr2.capacity();
			
			for ( TArray::const_reverse_iterator
				iter = arr2.rbegin(), end = arr2.rend();
				iter != end; ++iter )
			{
				if ( *iter != t - 1 )
					return false;
				
				--t;
			}
			
			if ( t != 0 )
				return false;
		}
	
		{
			while ( !arr1.empty() )
			{
				size_t t = arr1.back();
				arr1.pop_back();
				if ( t != arr1.size() )
					return false;
			}
			
			while ( !arr2.empty() )
			{
				size_t t = arr2.back();
				arr2.pop_back();
				if ( t != arr2.size() )
					return false;
			}
		}
	
		{
			arr1.resize(5);
	
			// 0 1 2 3 4
			for ( size_t i = 0; i < arr1.capacity(); ++i )
				arr1[i] = i;
	
			// 4 1 2 3
			arr1.erase_unordered(arr1.begin() + 0);
	
			// 4 1 3
			arr1.erase_unordered(arr1.begin() + 2);
	
			if ( arr1.at(0)  != 4 ) return false;
			if ( arr1.at(1)  != 1 ) return false;
			if ( arr1.at(2)  != 3 ) return false;
			if ( arr1.size() != 3 ) return false;
		}
	
		return true;
	}
	
	bool Test2()
	{
		typedef Array<size_t, 5> TArray1;
		TArray1 arr1;
		
		for ( size_t i = 0; i < arr1.capacity(); ++i )
			arr1.push_back(i);
		
		if ( !arr1.is_sorted() )
			return false;
		
		for ( TArray1::const_iterator iter = arr1.begin(), end = arr1.end();
			iter != end; ++iter )
		{
			TArray1::const_iterator finded_iter = arr1.linear_find(*iter);
			if ( *iter != *finded_iter )
				return false;
			
			TArray1::iterator finded_iter2 = arr1.linear_find(*iter);
			if ( *iter != *finded_iter2 )
				return false;
			
			TArray1::const_iterator finded_iter3 = arr1.binary_find(*iter);
			if ( *iter != *finded_iter3 )
				return false;
			
			TArray1::iterator finded_iter4 = arr1.binary_find(*iter);
			if ( *iter != *finded_iter4 )
				return false;
		}
		
		return true;
	}
	
	bool Test3()
	{
		typedef Array<size_t, 5> TArray1;
		TArray1 arr1;
		
		cpp::greater<TArray1::value_type> sorter;
		
		for ( size_t i = 0; i < arr1.capacity(); ++i )
			arr1.push_back(arr1.capacity() - i - 1);
		
		if ( !arr1.is_sorted(sorter) )
			return false;
		
		for ( TArray1::const_iterator
			iter = arr1.begin(), end = arr1.end();
			iter != end; ++iter )
		{
			TArray1::const_iterator finded_iter = arr1.linear_find(*iter);
			if ( *iter != *finded_iter )
				return false;
			
			TArray1::iterator finded_iter2 = arr1.linear_find(*iter);
			if ( *iter != *finded_iter2 )
				return false;
			
			TArray1::const_iterator finded_iter3 = arr1.binary_find(*iter, sorter);
			if ( *iter != *finded_iter3 )
				return false;
			
			TArray1::iterator finded_iter4 = arr1.binary_find(*iter, sorter);
			if ( *iter != *finded_iter4 )
				return false;
		}
		
		return true;
	}
	
	bool Test4()
	{
		typedef Array<size_t, 8> TArray1;
		TArray1 arr1;

		if ( 10 != *arr1.insert(arr1.begin() + 0, 10) ) return false; // 10
		if ( 11 != *arr1.insert(arr1.begin() + 0, 11) ) return false; // 11 10
		if ( 12 != *arr1.insert(arr1.begin() + 0, 12) ) return false; // 12 11 10
		if ( 13 != *arr1.insert(arr1.begin() + 1, 13) ) return false; // 12 13 11 10
		if ( 14 != *arr1.insert(arr1.begin() + 3, 14) ) return false; // 12 13 11 14 10
		if ( 15 != *arr1.insert(arr1.begin() + 3, 15) ) return false; // 12 13 11 15 14 10
		if ( 16 != *arr1.insert(arr1.begin() + 3, 16) ) return false; // 12 13 11 16 15 14 10
		if ( 99 != *arr1.insert(arr1.begin() + 7, 99) ) return false; // 12 13 11 16 15 14 10 99

		if ( arr1.at(0)  != 12 ) return false;
		if ( arr1.at(1)  != 13 ) return false;
		if ( arr1.at(2)  != 11 ) return false;
		if ( arr1.at(3)  != 16 ) return false;
		if ( arr1.at(4)  != 15 ) return false;
		if ( arr1.at(5)  != 14 ) return false;
		if ( arr1.at(6)  != 10 ) return false;
		if ( arr1.at(7)  != 99 ) return false;
		if ( arr1.size() !=  8 ) return false;
		
		arr1.resize(5);
		arr1.resize(2);
		arr1.resize(8);
		
		return true;
	}

	bool Test5()
	{
		typedef Array<size_t, 7> TArray1;
		TArray1 arr_tmp;

		arr_tmp.insert_unordered(arr_tmp.begin() + 0, 10); // 10
		arr_tmp.insert_unordered(arr_tmp.begin() + 0, 11); // 11 10
		arr_tmp.insert_unordered(arr_tmp.begin() + 0, 12); // 12 10 11
		arr_tmp.insert_unordered(arr_tmp.begin() + 1, 13); // 12 13 11 10
		arr_tmp.insert_unordered(arr_tmp.begin() + 3, 14); // 12 13 11 14 10
		arr_tmp.insert_unordered(arr_tmp.begin() + 3, 15); // 12 13 11 15 10 14
		arr_tmp.insert_unordered(arr_tmp.begin() + 3, 16); // 12 13 11 16 10 14 15

		// operator = test
		TArray1 arr1;
		arr1 = arr_tmp;
		
		if ( arr1.at(0)  != 12 ) return false;
		if ( arr1.at(1)  != 13 ) return false;
		if ( arr1.at(2)  != 11 ) return false;
		if ( arr1.at(3)  != 16 ) return false;
		if ( arr1.at(4)  != 10 ) return false;
		if ( arr1.at(5)  != 14 ) return false;
		if ( arr1.at(6)  != 15 ) return false;
		if ( arr1.size() !=  7 ) return false;
		
		return true;
	}
	
	bool Test6()
	{
		typedef Array<size_t, 7> TArray1;
		TArray1 arr_tmp;
		arr_tmp.push_sorted(10);
		arr_tmp.push_sorted(2);
		arr_tmp.push_sorted(15);
		arr_tmp.push_sorted(3);
		arr_tmp.push_sorted(99);
		arr_tmp.push_sorted(1);
		arr_tmp.push_sorted(2);
		
		// copy constructor test
		TArray1 arr1 = arr_tmp;
		
		if ( arr1.at(0)  !=  1 ) return false;
		if ( arr1.at(1)  !=  2 ) return false;
		if ( arr1.at(2)  !=  2 ) return false;
		if ( arr1.at(3)  !=  3 ) return false;
		if ( arr1.at(4)  != 10 ) return false;
		if ( arr1.at(5)  != 15 ) return false;
		if ( arr1.at(6)  != 99 ) return false;
		if ( arr1.size() !=  7 ) return false;
		
		return true;
	}
	
	bool Test7()
	{
		typedef Array<size_t, 10> TArray;
		TArray arr;
		arr.push_back(1);
		arr.push_back(2);
		arr.push_back(3);
		arr.push_back(4);
		arr.push_back(5);
		arr.push_back(6);
		arr.push_back(7);
		arr.push_back(8);
		arr.push_back(9);
		arr.push_back(10);
		for ( TArray::const_iterator
			iter = arr.begin(), end = arr.end();
			iter != end; )
		{
			if ( (*iter) % 2 )
				iter = arr.erase(iter);
			else
				++iter;
		}
		
		TArray arr2 = arr;
		
		if ( arr2.at(0)  !=  2 ) return false;
		if ( arr2.at(1)  !=  4 ) return false;
		if ( arr2.at(2)  !=  6 ) return false;
		if ( arr2.at(3)  !=  8 ) return false;
		if ( arr2.at(4)  != 10 ) return false;
		if ( arr2.size() !=  5 ) return false;
		
		return true;
	}
}

bool UtilsTest_Array()
{
	BME_LOG_INFO("UtilsTest_Array");
	using namespace utils_array_test;
	return
		Test0() && Test1() && Test2() && Test3() && Test4() && Test5() && Test6() && Test7();
}
