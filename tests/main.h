
#pragma once

#include "../precomp.h"

using namespace bme;
using namespace bme::fw3;

class Test
{
public:

	Test() {}
	virtual ~Test() {}

	virtual bool OnInit		() = 0;
	virtual void OnShutdown	() = 0;
	virtual bool OnUpdate	() = 0;
	virtual void OnRender	() = 0;
};

typedef boost::shared_ptr<Test> TestPtr;

class MyGame : public Application
{
	TestPtr	_test;

public:

	MyGame				();
	virtual ~MyGame		();

	bool	OnInit		();
	void	OnShutdown	();
	bool	OnUpdate	();
	void	OnRender	();
};
