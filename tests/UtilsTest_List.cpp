
#include "../precomp.h"
#include "UtilsTest.h"

#include <bme/utils/Iterator.h>
#include <bme/utils/List.h>
#include <bme/utils/Pool.h>

namespace utils_list_test
{
	bool Test1()
	{
		return true;
		
		// compile test
		
		typedef List<size_t,Allocator> TList;
		TList::allocator_type allocator;
		TList list1(allocator);
		
		TList::iterator       i1 = list1.begin();
		TList::const_iterator i2 = list1.begin();
		TList::const_iterator i3 = list1.cbegin();
		i1 = list1.end();
		i2 = list1.end();
		i3 = list1.cend();
		
		list1.push_back(2);
		list1.push_front(1);
		list1.pop_back();
		list1.pop_front();
		
		TList list2 = list1;
		list1 = list2;
		list2.swap(list1);
		
		list1.clear();
		bool is_empty = list1.empty();
		TList::size_type s = list1.size();
		if ( s != 0 || !is_empty )
			return false;

		TList::reference       r1 = list1.back();
		TList::const_reference r2 = list1.back();
		TList::reference       r3 = list1.front();
		TList::const_reference r4 = list1.front();
		
		i1 = list1.insert(i1, r1);
		i2 = list1.insert(i2, r1);
		i3 = list1.insert(i3, r1);
		i1 = list1.insert(i1, r2);
		i2 = list1.insert(i2, r2);
		i3 = list1.insert(i3, r2);
		i1 = list1.insert(i1, r3);
		i2 = list1.insert(i2, r3);
		i3 = list1.insert(i3, r3);
		i1 = list1.insert(i1, r4);
		i2 = list1.insert(i2, r4);
		i3 = list1.insert(i3, r4);
		
		list1.erase(i1);
		list1.erase(i2);
		list1.erase(i3);
		
		return true;
	}
	
	bool Test2()
	{
		typedef List<size_t> TList;
		TList list2;
		list2.push_back(1);
		list2.push_back(2);
		list2.push_back(3);
		TList list = list2;
		cpp::swap(list, list2);
		
		for ( TList::iterator
			iter = list.begin(), end = list.end();
			iter != end; )
		{
			if ( 2 == *iter )
				iter = list.erase(iter);
			else
				++iter;
		}
		
		if ( 2 != list.size() )
			return false;
		
		if ( 1 != list.front() )
			return false;
		
		if ( 3 != list.back() )
			return false;
		
		return true;
	}
	
	bool Test3()
	{
		typedef List<size_t> TList;
		TList list;
		list.insert(list.begin(), 2);
		if ( list.back() != 2 || list.front() != 2 ) return false;
		list.clear();
		if ( !list.empty() || list.size() != 0 ) return false;
		
		for ( TList::const_iterator
			iter = list.begin(), end = list.end();
			iter != end; ++iter ) {}
		
		return true;
	}
	
	bool Test4()
	{
		typedef List<size_t> TList;
		
		TList orig_list;
		orig_list.push_back(1);
		orig_list.push_back(2);
		orig_list.push_back(3);
		orig_list.push_back(4);
		orig_list.push_back(5);
			
		{
			TList list = orig_list;
			if ( 3 != *list.erase(++list.begin()) )
				return false;
			if ( 2 != *list.insert(++list.begin(), 2) )
				return false;
			
			if ( list != orig_list )
				return false;
		}
		
		{
			
		}
		
		
		return true;
	}
	
	bool Test5()
	{
		return true;
	}
}

bool UtilsTest_List()
{
	BME_LOG_INFO("UtilsTest_List");
	using namespace utils_list_test;
	return Test1() && Test2() && Test3() && Test4() && Test5();
}
