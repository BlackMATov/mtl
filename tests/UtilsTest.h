
#pragma once

#include "main.h"

bool UtilsTest_Array();
bool UtilsTest_Pool();
bool UtilsTest_List();
bool UtilsTest_Iterator();

class AutoTimer : private cpp::noncopyable
{
	StrId   _name;
	clock_t _clock_time;
	clock_t _last_time;
	
public:
	
	AutoTimer(const char* name = "noname", clock_t last_time = 0);
	~AutoTimer();
	clock_t getTime() const;
};

class UtilsTest : public Test
{
public:

	UtilsTest() {}
	virtual ~UtilsTest() {}

	virtual bool OnInit		();
	virtual void OnShutdown	();
	virtual bool OnUpdate	();
	virtual void OnRender	();
};
