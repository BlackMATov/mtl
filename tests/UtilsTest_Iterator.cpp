
#include "../precomp.h"
#include "UtilsTest.h"

#include <bme/utils/Iterator.h>
#include <bme/utils/List.h>

namespace utils_iterator_test
{
	struct TestStruct
	{
		char buf[64];
	};

	bool Test1()
	{
		return true;
		
		// compile test
		
		typedef TestStruct*                     iterator_type;
		typedef TestStruct*                     iterator_type2;
		typedef std::reverse_iterator<iterator_type>  reverse_iterator_type;
		typedef std::reverse_iterator<iterator_type2> reverse_iterator_type2;
		
		iterator_type i1;
		reverse_iterator_type ri1(i1);
		reverse_iterator_type ri2;
		reverse_iterator_type ri3(ri2);
		ri3 = ri1;
		reverse_iterator_type ri4(reverse_iterator_type2());
		ri3 = reverse_iterator_type2();
		
		(*ri1).buf[1] = 'a';
		ri1->buf[1]   = 'b';
		
		++ri1;
		++ri1;
		ri1++;
		ri1++;
		
		ri1 == ri2;
		ri1 != ri2;
		ri1 <  ri2;
		ri1 <= ri2;
		ri1 >  ri2;
		ri1 >= ri2;
		
		reverse_iterator_type::difference_type
		d1 = std::distance(ri2, ri1);
		std::advance(ri2, d1);
	}
	
	bool Test2()
	{
		return true;
	}
	
	bool Test3()
	{
		return true;
	}
	
	bool Test4()
	{
		return true;
	}
	
	bool Test5()
	{
		return true;
	}
}

bool UtilsTest_Iterator()
{
	BME_LOG_INFO("UtilsTest_Iterator");
	using namespace utils_iterator_test;
	return Test1() && Test2() && Test3() && Test4() && Test5();
}
