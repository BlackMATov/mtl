
#include "../precomp.h"
#include "UtilsTest.h"

// ------------------------------------

#define STL_TESTS
//#define BOOST_TESTS
#define BME_TESTS

//#define BIG_TESTS
#define SMALL_TESTS
#define ST_ENABLE_TESTS
//#define MT_ENABLE_TESTS
#define MT_4_THREADS_TESTS

// ------------------------------------

#ifdef BOOST_TESTS
#	ifndef MT_ENABLE_TESTS
#		define BOOST_POOL_NO_MT
#	endif
#	include <boost/pool/pool_alloc.hpp>
#	include <boost/pool/object_pool.hpp>
#	include <boost/pool/singleton_pool.hpp>
#endif

// ------------------------------------

namespace utils_pool_test
{
	static char ttt = 0;
	
	struct BigStruct
	{
		char dummy[64];
		
		BigStruct()
		{
			dummy[0] = ++ttt;
			dummy[1] = ++ttt;
			dummy[2] = ++ttt;
		}
		
		BigStruct(const BigStruct& other)
		{
			dummy[0] = other.dummy[0];
			dummy[1] = other.dummy[1];
			dummy[2] = other.dummy[2];
		}
		
		bool operator == (const BigStruct& other) const
		{
			return
				dummy[0] == other.dummy[0] &&
				dummy[1] == other.dummy[1] &&
				dummy[2] == other.dummy[2];
		}
	};
	
	template < typename TStringList >
	void test1(size_t samples_count, size_t strings_count)
	{
		TStringList list1;
		
		for ( size_t sample = 0; sample < samples_count; ++sample )
		{
			list1.clear();
			
			for ( size_t i = 0; i < strings_count; ++i )
				list1.push_back(typename TStringList::value_type());
			
			TStringList list2 = list1;
			
			if ( !(list2 == list1) )
				throw std::exception();
		}
	}
	
	template < typename TListChar, typename TListListChar >
	void test2(size_t samples_count, size_t list_and_char_count)
	{
		for ( size_t sample = 0; sample < samples_count; ++sample )
		{
			TListListChar llc;
			
			for ( size_t i = 0; i < list_and_char_count; ++i )
			{
				TListChar lc;
				for ( size_t j = 0; j < list_and_char_count; ++j )
				{
					lc.push_back('w');
				}
				llc.push_back(lc);
			}
		}
	}
	
	template < typename TStringList >
	void test3(size_t samples_count, size_t strings_count)
	{
		bme::Thread thread1(bme::cpp::bind(&test1<TStringList>, samples_count, strings_count));
		bme::Thread thread2(bme::cpp::bind(&test1<TStringList>, samples_count, strings_count));
	#ifdef MT_4_THREADS_TESTS
		bme::Thread thread3(bme::cpp::bind(&test1<TStringList>, samples_count, strings_count));
		bme::Thread thread4(bme::cpp::bind(&test1<TStringList>, samples_count, strings_count));
	#endif
		thread1.Join();
		thread2.Join();
	#ifdef MT_4_THREADS_TESTS
		thread3.Join();
		thread4.Join();
	#endif
	}
	
	template < typename TListChar, typename TListListChar >
	void test4(size_t samples_count, size_t list_and_char_count)
	{
		bme::Thread thread1(bme::cpp::bind(&test2<TListChar,TListListChar>, samples_count, list_and_char_count));
		bme::Thread thread2(bme::cpp::bind(&test2<TListChar,TListListChar>, samples_count, list_and_char_count));
	#ifdef MT_4_THREADS_TESTS
		bme::Thread thread3(bme::cpp::bind(&test2<TListChar,TListListChar>, samples_count, list_and_char_count));
		bme::Thread thread4(bme::cpp::bind(&test2<TListChar,TListListChar>, samples_count, list_and_char_count));
	#endif
		thread1.Join();
		thread2.Join();
	#ifdef MT_4_THREADS_TESTS
		thread3.Join();
		thread4.Join();
	#endif
	}
	
	void start_section(const char* name)
	{
		std::cout << name << std::endl;
	}
}

bool UtilsTest_Pool()
{
	BME_LOG_INFO("UtilsTest_Pool");
	using namespace utils_pool_test;
	
	clock_t test1_time  = 0;
	clock_t test2_time  = 0;
	clock_t test3_time  = 0;
	clock_t test4_time  = 0;

	size_t test1_value1 = 4000;
	size_t test1_value2 = 500;
	size_t test2_value1 = 40;
	size_t test2_value2 = 300;

#ifdef BIG_TESTS
	test1_value1 *= 2;
	test1_value2 *= 2;
	test2_value1 *= 2;
	test2_value2 *= 2;
#endif

#ifdef SMALL_TESTS
	test1_value1 /= 10;
	test1_value2 /= 10;
	test2_value1 /= 10;
	test2_value2 /= 10;
#endif
	
#ifdef STL_TESTS
	start_section("-=STL=-");
	{
		typedef BigStruct            TString;
		typedef std::list<TString>   TStringList;
		typedef std::list<char>      TListChar;
		typedef std::list<TListChar> TListListChar;
		
		#ifdef ST_ENABLE_TESTS
		{
			AutoTimer auto_timer("test1");
			test1<TStringList>(test1_value1, test1_value2);
			test1_time = auto_timer.getTime();
		}
		
		{
			AutoTimer auto_timer("test2");
			test2<TListChar, TListListChar>(test2_value1, test2_value2);
			test2_time = auto_timer.getTime();
		}
		#endif

		#ifdef MT_ENABLE_TESTS
		{
			AutoTimer auto_timer("mt_test1");
			test3<TStringList>(test1_value1, test1_value2);
			test3_time = auto_timer.getTime();
		}

		{
			AutoTimer auto_timer("mt_test2");
			test4<TListChar, TListListChar>(test2_value1, test2_value2);
			test4_time = auto_timer.getTime();
		}
		#endif
	}
#endif
	
#ifdef BOOST_TESTS
	start_section("-=boost_pool=-");
	{
		typedef BigStruct                                                    TString;
		typedef std::list<TString, boost::fast_pool_allocator<TString> >     TStringList;
		typedef std::list<char, boost::fast_pool_allocator<char> >           TListChar;
		typedef std::list<TListChar, boost::fast_pool_allocator<TListChar> > TListListChar;
		
		#ifdef ST_ENABLE_TESTS
		{
			AutoTimer auto_timer("test1", test1_time);
			test1<TStringList>(test1_value1, test1_value2);
		}
		
		{
			AutoTimer auto_timer("test2", test2_time);
			test2<TListChar, TListListChar>(test2_value1, test2_value2);
		}
		#endif

		#ifdef MT_ENABLE_TESTS
		{
			AutoTimer auto_timer("mt_test1", test3_time);
			test3<TStringList>(test1_value1, test1_value2);
		}

		{
			AutoTimer auto_timer("mt_test2", test4_time);
			test4<TListChar, TListListChar>(test2_value1, test2_value2);
		}
		#endif
	}
#endif

#ifdef BME_TESTS
	start_section("-=bme_pool=-");
	{
	#ifdef MT_ENABLE_TESTS
		typedef BigStruct                                                                  TString;
		typedef std::list<TString, bme::PoolSTLAllocator<TString, 100, bme::Pool_MT> >     TStringList;
		typedef std::list<char, bme::PoolSTLAllocator<char, 100, bme::Pool_MT> >           TListChar;
		typedef std::list<TListChar, bme::PoolSTLAllocator<TListChar, 100, bme::Pool_MT> > TListListChar;
	#else
		typedef BigStruct                                                                  TString;
		typedef std::list<TString, bme::PoolSTLAllocator<TString, 100, bme::Pool_ST> >     TStringList;
		typedef std::list<char, bme::PoolSTLAllocator<char, 100, bme::Pool_ST> >           TListChar;
		typedef std::list<TListChar, bme::PoolSTLAllocator<TListChar, 100, bme::Pool_ST> > TListListChar;
	#endif
		
		#ifdef ST_ENABLE_TESTS
		{
			AutoTimer auto_timer("test1", test1_time);
			test1<TStringList>(test1_value1, test1_value2);
		}
		
		{
			AutoTimer auto_timer("test2", test2_time);
			test2<TListChar, TListListChar>(test2_value1, test2_value2);
		}
		#endif
		
		#ifdef MT_ENABLE_TESTS
		{
			AutoTimer auto_timer("mt_test1", test3_time);
			test3<TStringList>(test1_value1, test1_value2);
		}
		
		{
			AutoTimer auto_timer("mt_test2", test4_time);
			test4<TListChar, TListListChar>(test2_value1, test2_value2);
		}
		#endif
	}
#endif
		
	return true;
}
