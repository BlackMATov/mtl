#include <mtl/array.hpp>
#include <vector>

namespace random
{
	static unsigned int g_seed = 0;
	void set_seed(unsigned int seed)
	{
		g_seed = seed;
	}
	int gen_int(int min, int max)
	{
		g_seed = 214013 * g_seed + 2531011;
		return min + (g_seed ^ g_seed >> 15) % (max - min + 1);
	}
}

static int nnnn = 0;

struct TestClass2
{
	int v;
	int v2;
	int v3;
	int v4;
	TestClass2() : v(0), v2(0), v3(0), v4(0) { ++nnnn; }
	TestClass2(int v) : v(v), v2(v), v3(v), v4(v) { ++nnnn; }
	TestClass2(const TestClass2& other) : v(other.v), v2(other.v2), v3(other.v3), v4(other.v4) { ++nnnn; }
	TestClass2& operator=(const TestClass2& other)
	{
		v = other.v;
		v2 = other.v2;
		v3 = other.v3;
		v4 = other.v4;
		++nnnn;
		return *this;
	}
	~TestClass2() {}
};

struct TestClassBase
{
	virtual ~TestClassBase() {}
};

struct TestClass : TestClassBase
{
	//TestClass2 v2;
	int v;
	TestClass() : v(0) { ++nnnn; }
	TestClass(int v) : v(v) { ++nnnn; }
	TestClass(const TestClass& other) : v(other.v) { ++nnnn; }
	TestClass& operator=(const TestClass& other)
	{
		v = other.v;
		++nnnn;
		return *this;
	}
	virtual ~TestClass() {}
};

typedef std::vector<TestClass> TSTLVec;
typedef mtl::array<TestClass, mtl::allocator> TMyVec;

namespace mtl { namespace tests
{
	// insert_to_index
	
	inline void insert_to_index(TSTLVec& vec, size_t index, const TSTLVec::value_type& value)
	{
		vec.insert(vec.begin() + index, value);
	}
	
	inline void insert_to_index(TMyVec& vec, size_t index, const TMyVec::value_type& value)
	{
		vec.insert(index, value);
	}
	
	// insert_n_to_index
	
	inline void insert_n_to_index(TSTLVec& vec, size_t index, size_t n, const TSTLVec::value_type& value)
	{
		vec.insert(vec.begin() + index, n, value);
	}
	
	inline void insert_n_to_index(TMyVec& vec, size_t index, size_t n, const TMyVec::value_type& value)
	{
		vec.insert(index, n, value);
	}
	
	// erase_from_index
	
	inline void erase_from_index(TSTLVec& vec, size_t index, size_t n)
	{
		if ( n > 1 )
		{
			vec.erase(vec.begin() + index, vec.begin() + index + n);
		}
		else
		{
			vec.erase(vec.begin() + index);
		}
	}
	
	inline void erase_from_index(TMyVec& vec, size_t index, size_t n)
	{
		if ( n > 1 )
		{
			vec.erase(index, index + n);
		}
		else
		{
			vec.erase(index);
		}
	}
	
	// array_speed_test_N
	
	template < typename T >
	void array_speed_test_1()
	{
		T vec;
		vec.resize(10);
		for ( size_t i = 0; i < 60000; ++i )
		{
			size_t index = random::gen_int(0, vec.size());
			index = 2;
			size_t value = random::gen_int(0, 10000);
			insert_to_index(vec, index, value);
		}
		vec.clear();
	}

	template < typename T >
	void array_speed_test_2()
	{
		nnnn = 0;
		T vec;
		for ( size_t i = 0; i < 20000; ++i )
		{
			size_t index = random::gen_int(0, vec.size());
			size_t value = random::gen_int(0, 10000);
			size_t n     = random::gen_int(1, 30);
			insert_n_to_index(vec, index, n, value);
		}
		vec.clear();
		//::printf("nnnn: %i\n", nnnn);
	}

	template < typename T >
	void array_speed_test_3()
	{
		nnnn = 0;
		T vec;
		for ( size_t i = 0; i < 70000; ++i )
		{
			vec.resize(2000, 5);
			while ( !vec.empty() )
			{
				size_t index = random::gen_int(0, vec.size());
				size_t n     = random::gen_int(1, 100);
				index = std::min(index, vec.size() - n);
				index = std::max(index, size_t(0));
				n     = std::min(n, vec.size() - index);
				if ( n > 0 )
					erase_from_index(vec, index, n);
			}
		}
		vec.clear();
		//::printf("nnnn: %i\n", nnnn);
	}
	
	template < typename T >
	void array_speed_test_4()
	{
		nnnn = 0;
		T vec;
		for ( size_t i = 0; i < 50000000; ++i )
		{
			size_t value = random::gen_int(0, 10000);
			vec.push_back(value);
		}
		while ( !vec.empty() )
		{
			vec.pop_back();
		}
		vec.clear();
		//::printf("nnnn: %i\n", nnnn);
	}
	
	bool array_speed_test(bool stl, size_t num)
	{
		random::set_seed(0);
		if ( stl )
		{
			switch ( num )
			{
			case 0:
				array_speed_test_1<TSTLVec>();
				return true;
			case 1:
				array_speed_test_2<TSTLVec>();
				return true;
			case 2:
				array_speed_test_3<TSTLVec>();
				return true;
			case 3:
				array_speed_test_4<TSTLVec>();
				return true;
			default:
				return false;
			}
		}
		else
		{
			typedef mtl::array<int, mtl::allocator> TVec;
			switch ( num )
			{
			case 0:
				array_speed_test_1<TMyVec>();
				return true;
			case 1:
				array_speed_test_2<TMyVec>();
				return true;
			case 2:
				array_speed_test_3<TMyVec>();
				return true;
			case 3:
				array_speed_test_4<TMyVec>();
				return true;
			default:
				return false;
			}
		}
	}
	
}} // namespace mtl::tests
