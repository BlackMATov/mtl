#include "main.hpp"

#include <conio.h>
#include <ctime>

// ----------------------------------------------------------------------------
//
// AutoTimer
//
// ----------------------------------------------------------------------------

class AutoTimer
{
	AutoTimer(const AutoTimer&);
	AutoTimer& operator=(const AutoTimer&);
	const char* _name;
	clock_t     _clock_time;
	clock_t     _last_time;
public:
	AutoTimer(const char* name = "noname", clock_t last_time = 0);
	~AutoTimer();
	clock_t getTime() const;
};

AutoTimer::AutoTimer(const char* name, clock_t last_time)
: _name(name), _clock_time(::clock()), _last_time(last_time)
{
}

AutoTimer::~AutoTimer()
{
	clock_t clock_time_2 = getTime();
	::printf("%s: %i", _name, static_cast<int>(clock_time_2));
	if ( _last_time && clock_time_2 > 0 )
		::printf(" x%f\n", (double)_last_time / (double)clock_time_2);
	else
		::printf("\n");
}

clock_t AutoTimer::getTime() const
{
	return ::clock() - _clock_time;
}

void start_test(const char* name, bool (*func)())
{
	::printf("test '%s' ", name);
	if ( func() )
		::printf(" succeed.\n");
	else
		::printf(" error!\n");
}

void start_speed_test(const char* name, bool (*func)(bool, size_t))
{
	for ( size_t i = 0; i < 10; ++i )
	{
		clock_t test1_time = 0;
		{
			AutoTimer auto_timer(name);
			func(true, i);
			test1_time = auto_timer.getTime();
		}
		{
			AutoTimer auto_timer(name, test1_time);
			if ( !func(false, i) )
				break;
		}
	}
}

int main()
{
	using namespace mtl::tests;
	start_test("allocator_test", &allocator_test);
	start_test("array_test", &array_test);
	
	start_speed_test("array_speed_test", &array_speed_test);
	::printf("\n\nPress Enter to exit...");
	_getch();
	return 0;
}
