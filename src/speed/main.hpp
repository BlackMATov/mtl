#include <mtl/internal/base.hpp>

namespace mtl { namespace tests
{
	bool allocator_test();
	
	bool array_test();
	bool array_speed_test(bool stl, size_t);
}} // namespace mtl::tests
