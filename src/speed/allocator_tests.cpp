#include <mtl/allocator.hpp>

namespace mtl { namespace tests
{
	void allocator_dummy()
	{
		mtl::allocator a1;
		mtl::allocator a2(a1);
		a1 = a2;
		
		void* m1 = a1.allocate(10);
		a1.deallocate(m1, 10);
	}
	
	bool allocator_test()
	{
		return true;
	}
}} // namespace mtl::tests
