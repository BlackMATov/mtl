#include "tests.hpp"

// ----------------------------------------------------------------------------
// 
// test class
// 
// ----------------------------------------------------------------------------

struct update_tag {};
struct render_tag {};

class TestListClass
: public mtl::ilist_hook<update_tag>::type
, public mtl::ilist_hook<render_tag>::type
{
	int _i;
public:
	TestListClass(int i = 0)
	: _i(i){
	}
	
	TestListClass(const TestListClass& other)
	: mtl::ilist_hook<update_tag>::type(other)
	, mtl::ilist_hook<render_tag>::type(other)
	, _i(other._i){
	}
	
	TestListClass& operator=(const TestListClass& other){
		if ( this != &other ) {
			_i = other._i;
		}
		return *this;
	}
	
	~TestListClass(){
	}
	
	bool operator<(const TestListClass& other) const{
		return _i < other._i;
	}
	
	int get_value() const {
		return _i;
	}
	
	template < typename TAG >
	bool is_linked() const{
		typedef mtl::_ilist::hook<TAG> base_type;
		typedef mtl::_ilist::algo<TAG> algo;
		const base_type* node = static_cast<const base_type*>(this);
		return algo::is_linked(node);
	}
	
	bool operator == (int i) const { return _i == i; }
	bool operator != (int i) const { return _i != i; }
	bool operator == (const TestListClass& other) const { return _i == other._i; }
	bool operator != (const TestListClass& other) const { return _i != other._i; }
};

typedef mtl::ilist<TestListClass, update_tag> TList;

// ----------------------------------------------------------------------------
// 
// helpers
// 
// ----------------------------------------------------------------------------

template < typename T >
static T iterator_advance(T iter, size_t n){
	for ( size_t i = 0; i < n; ++i ) {
		++iter;
	}
	return iter;
}

template < typename T >
static size_t iterator_distance(T first, T last){
	size_t n = 0;
	while ( first != last ) {
		++first;
		++n;
	}
	return n;
}

template < typename T, typename TAG >
static typename mtl::ilist<T,TAG>::iterator get_iter_to(T& v)
{
	typedef mtl::ilist<T,TAG> l;
	return l::iterator_to(v);
}

template < typename T, typename TAG >
static typename mtl::ilist<T,TAG>::const_iterator get_const_iter_to(const T& v)
{
	typedef mtl::ilist<T,TAG> l;
	return l::iterator_to(v);
}

template < typename T >
static void fill_n_list(T& l, size_t n){
	for ( size_t i = 0; i < n; ++i ) {
		TestListClass* obj = new TestListClass(i+1);
		l.push_back(*obj);
	}
}

static void delete_disposer(TestListClass* obj){
	delete obj;
}

#define MTL_CHECK_BORDER(_arr, _front, _back)\
{\
	CHECK(_arr.front() == _front);\
	CHECK(_arr.back()  == _back);\
}

#define MTL_CHECK_SIZE(_arr, _size)\
{\
	CHECK(_arr.size() == _size);\
}

#define MTL_CHECK_ELEMS_1(_list, _e0)\
	CHECK(*iterator_advance(_list.begin(), 0) == _e0);

#define MTL_CHECK_ELEMS_2(_list, _e0, _e1)\
	MTL_CHECK_ELEMS_1(_list, _e0);\
	CHECK(*iterator_advance(_list.begin(), 1) == _e1);

#define MTL_CHECK_ELEMS_3(_list, _e0, _e1, _e2)\
	MTL_CHECK_ELEMS_2(_list, _e0, _e1);\
	CHECK(*iterator_advance(_list.begin(), 2) == _e2);

#define MTL_CHECK_ELEMS_4(_list, _e0, _e1, _e2, _e3)\
	MTL_CHECK_ELEMS_3(_list, _e0, _e1, _e2);\
	CHECK(*iterator_advance(_list.begin(), 3) == _e3);

#define MTL_CHECK_ELEMS_5(_list, _e0, _e1, _e2, _e3, _e4)\
	MTL_CHECK_ELEMS_4(_list, _e0, _e1, _e2, _e3);\
	CHECK(*iterator_advance(_list.begin(), 4) == _e4);

#define MTL_CHECK_ELEMS_6(_list, _e0, _e1, _e2, _e3, _e4, _e5)\
	MTL_CHECK_ELEMS_5(_list, _e0, _e1, _e2, _e3, _e4);\
	CHECK(*iterator_advance(_list.begin(), 5) == _e5);

#define MTL_CHECK_ELEMS_7(_list, _e0, _e1, _e2, _e3, _e4, _e5, _e6)\
	MTL_CHECK_ELEMS_6(_list, _e0, _e1, _e2, _e3, _e4, _e5);\
	CHECK(*iterator_advance(_list.begin(), 6) == _e6);

#define MTL_CHECK_ELEMS_8(_list, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7)\
	MTL_CHECK_ELEMS_7(_list, _e0, _e1, _e2, _e3, _e4, _e5, _e6);\
	CHECK(*iterator_advance(_list.begin(), 7) == _e7);

#define MTL_CHECK_ELEMS_9(_list, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7, _e8)\
	MTL_CHECK_ELEMS_8(_list, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7);\
	CHECK(*iterator_advance(_list.begin(), 8) == _e8);

// ----------------------------------------------------------------------------
// 
// tests
// 
// ----------------------------------------------------------------------------

TEST(ilist_operators_compile_test)
{
	TList i1;
	TList i2;
	i1 == i2;
	i1 != i2;
	i1 <  i2;
	i1 <= i2;
	i1 >  i2;
	i1 >= i2;
}

TEST(ilist_iterator_compile_test)
{
	typedef TList::iterator       it;
	typedef TList::const_iterator cit;
	{
		it  i1;
		cit i2;
		(void)i1;(void)i2;
	}
	{
		it  i1 = it (0);
		cit i2 = it (0);
		cit i3 = cit(0);
		(void)i1;(void)i2;(void)i3;
	}
	{
		it  i1;
		cit i2;
		cit i3;
		i1 = it(0);
		i2 = it(0);
		i3 = cit(0);
	}
	{
		TList::value_type n1;
		TList::value_type n2;
		it            i1(&n1);
		cit           i2(&n2);
		CHECK( (i1 == i1));
		CHECK(!(i1 != i1));
		
		CHECK( (i2 == i2));
		CHECK(!(i2 != i2));
		
		CHECK(!(i1 == i2));
		CHECK( (i1 != i2));
		
		CHECK(!(i2 == i1));
		CHECK( (i2 != i1));
		
		CHECK( it(&n1) == cit(&n1));
		CHECK(cit(&n1) ==  it(&n1));
	}
}

TEST(ilist_reverse_iterator_compile_test)
{
	typedef TList::reverse_iterator       it;
	typedef TList::const_reverse_iterator cit;
	{
		it  i1;
		cit i2;
		(void)i1;(void)i2;
	}
	{
		it  i1 = it (0);
		cit i2 = it (0);
		cit i3 = cit(0);
		(void)i1;(void)i2;(void)i3;
	}
	{
		it  i1;
		cit i2;
		cit i3;
		i1 = it(0);
		i2 = it(0);
		i3 = cit(0);
	}
	{
		TList::value_type n1;
		TList::value_type n2;
		it            i1(&n1);
		cit           i2(&n2);
		CHECK( (i1 == i1));
		CHECK(!(i1 != i1));
		
		CHECK( (i2 == i2));
		CHECK(!(i2 != i2));
		
		CHECK(!(i1 == i2));
		CHECK( (i1 != i2));
		
		CHECK(!(i2 == i1));
		CHECK( (i2 != i1));
		
		CHECK( it(&n1) == cit(&n1));
		CHECK(cit(&n1) ==  it(&n1));
	}
}

TEST(ilist_iterator_test)
{
	{
		TList l;
		TList::value_type buf[] = {1,2,3,4,5,6};
		l.assign(buf, buf + sizeof(buf)/sizeof(buf[0]));
		TList::iterator i = l.begin();
		CHECK(*i == 1); CHECK(i->get_value() == 1); ++i;
		CHECK(*i == 2); CHECK(i->get_value() == 2); ++i;
		CHECK(*i == 3); CHECK(i->get_value() == 3); ++i;
		CHECK(*i == 4); CHECK(i->get_value() == 4); ++i;
		CHECK(*i == 5); CHECK(i->get_value() == 5); ++i;
		CHECK(*i == 6); CHECK(i->get_value() == 6); ++i;
		CHECK(i == l.end());
		TList::const_iterator ci = l.cbegin();
		CHECK(*ci == 1); CHECK(ci->get_value() == 1); ++ci;
		CHECK(*ci == 2); CHECK(ci->get_value() == 2); ++ci;
		CHECK(*ci == 3); CHECK(ci->get_value() == 3); ++ci;
		CHECK(*ci == 4); CHECK(ci->get_value() == 4); ++ci;
		CHECK(*ci == 5); CHECK(ci->get_value() == 5); ++ci;
		CHECK(*ci == 6); CHECK(ci->get_value() == 6); ++ci;
		CHECK(ci == l.cend());
	}
}

TEST(ilist_reverse_iterator_test)
{
	{
		TList l;
		TList::value_type buf[] = {1,2,3,4,5,6};
		l.assign(buf, buf + sizeof(buf)/sizeof(buf[0]));
		TList::reverse_iterator ri = l.rbegin();
		CHECK(*ri == 6); CHECK(ri->get_value() == 6); ++ri;
		CHECK(*ri == 5); CHECK(ri->get_value() == 5); ++ri;
		CHECK(*ri == 4); CHECK(ri->get_value() == 4); ++ri;
		CHECK(*ri == 3); CHECK(ri->get_value() == 3); ++ri;
		CHECK(*ri == 2); CHECK(ri->get_value() == 2); ++ri;
		CHECK(*ri == 1); CHECK(ri->get_value() == 1); ++ri;
		CHECK(ri == l.rend());
		TList::const_reverse_iterator cri = l.crbegin();
		CHECK(*cri == 6); CHECK(cri->get_value() == 6); ++cri;
		CHECK(*cri == 5); CHECK(cri->get_value() == 5); ++cri;
		CHECK(*cri == 4); CHECK(cri->get_value() == 4); ++cri;
		CHECK(*cri == 3); CHECK(cri->get_value() == 3); ++cri;
		CHECK(*cri == 2); CHECK(cri->get_value() == 2); ++cri;
		CHECK(*cri == 1); CHECK(cri->get_value() == 1); ++cri;
		CHECK(cri == l.crend());
	}
}

TEST(ilist_swap_test)
{
	{
		TList l1;
		TList l2;
		TList::value_type buf[] = {1,2,3,4,5,6,7};
		l1.assign(buf+0, buf+3);
		l2.assign(buf+3, buf+7);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_ELEMS_4(l2,4,5,6,7);
		MTL_CHECK_SIZE(l2,4);
		l1.swap(l2);
		MTL_CHECK_ELEMS_4(l1,4,5,6,7);
		MTL_CHECK_SIZE(l1,4);
		MTL_CHECK_ELEMS_3(l2,1,2,3);
		MTL_CHECK_SIZE(l2,3);
	}
	{
		TList l1;
		TList l2;
		TList::value_type buf[] = {1,2,3,4,5,6,7};
		l1.assign(buf+0, buf+4);
		l2.assign(buf+4, buf+7);
		MTL_CHECK_ELEMS_4(l1,1,2,3,4);
		MTL_CHECK_SIZE(l1,4);
		MTL_CHECK_ELEMS_3(l2,5,6,7);
		MTL_CHECK_SIZE(l2,3);
		l1.swap(l2);
		MTL_CHECK_ELEMS_3(l1,5,6,7);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_ELEMS_4(l2,1,2,3,4);
		MTL_CHECK_SIZE(l2,4);
	}
	{
		TList l1;
		TList l2;
		TList::value_type buf[] = {1,2,3,4,5,6,7};
		l1.assign(buf+0, buf+3);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_SIZE(l2,0);
		l1.swap(l2);
		MTL_CHECK_SIZE(l1,0);
		MTL_CHECK_ELEMS_3(l2,1,2,3);
		MTL_CHECK_SIZE(l2,3);
	}
	{
		TList l1;
		TList l2;
		TList::value_type buf[] = {1,2,3,4,5,6,7};
		l2.assign(buf+0, buf+3);
		MTL_CHECK_SIZE(l1,0);
		MTL_CHECK_ELEMS_3(l2,1,2,3);
		MTL_CHECK_SIZE(l2,3);
		l1.swap(l2);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_SIZE(l2,0);
	}
	{
		TList l1;
		TList l2;
		l1.swap(l2);
		MTL_CHECK_SIZE(l1,0);
		MTL_CHECK_SIZE(l2,0);
		TList::value_type buf[] = {1,2,3,4,5,6,7};
		l1.assign(buf+0, buf+3);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_SIZE(l2,0);
		l1.swap(l1);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_SIZE(l2,0);
	}
}

TEST(ilist_push_and_pop_test)
{
	{
		TList l;
		TList::value_type v1(1);
		TList::value_type v2(2);
		TList::value_type v3(3);
		l.push_back(v1);
		MTL_CHECK_ELEMS_1(l,1);
		MTL_CHECK_BORDER(l,1,1);
		MTL_CHECK_SIZE(l,1);
		l.push_back(v2);
		MTL_CHECK_ELEMS_2(l,1,2);
		MTL_CHECK_BORDER(l,1,2);
		MTL_CHECK_SIZE(l,2);
		l.push_back(v3);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_BORDER(l,1,3);
		MTL_CHECK_SIZE(l,3);
		l.pop_back();
		MTL_CHECK_ELEMS_2(l,1,2);
		MTL_CHECK_BORDER(l,1,2);
		MTL_CHECK_SIZE(l,2);
		l.pop_front();
		MTL_CHECK_ELEMS_1(l,2);
		MTL_CHECK_BORDER(l,2,2);
		MTL_CHECK_SIZE(l,1);
		l.pop_front();
		MTL_CHECK_SIZE(l,0);
		CHECK(!v1.is_linked<update_tag>());
		CHECK(!v2.is_linked<update_tag>());
		CHECK(!v3.is_linked<update_tag>());
		l.push_front(v1);
		MTL_CHECK_ELEMS_1(l,1);
		MTL_CHECK_BORDER(l,1,1);
		MTL_CHECK_SIZE(l,1);
		l.pop_back();
		MTL_CHECK_SIZE(l,0);
		CHECK(!v1.is_linked<update_tag>());
		CHECK(!v2.is_linked<update_tag>());
		CHECK(!v3.is_linked<update_tag>());
	}
}

TEST(ilist_pop_dispose_test)
{
	TList l;
	l.push_back(*(new TList::value_type(1)));
	l.push_back(*(new TList::value_type(2)));
	l.push_back(*(new TList::value_type(3)));
	MTL_CHECK_ELEMS_3(l,1,2,3);
	MTL_CHECK_BORDER(l,1,3);
	MTL_CHECK_SIZE(l,3);
	l.pop_back_and_dispose(&delete_disposer);
	MTL_CHECK_ELEMS_2(l,1,2);
	MTL_CHECK_BORDER(l,1,2);
	MTL_CHECK_SIZE(l,2);
	l.pop_front_and_dispose(&delete_disposer);
	MTL_CHECK_ELEMS_1(l,2);
	MTL_CHECK_BORDER(l,2,2);
	MTL_CHECK_SIZE(l,1);
	l.pop_back_and_dispose(&delete_disposer);
	MTL_CHECK_SIZE(l,0);
}

TEST(ilist_insert_test)
{
	{
		TList l;
		TList::value_type v1(1);
		TList::value_type v2(2);
		TList::value_type v3(3);
		CHECK(*l.insert(l.begin(), v1) == 1);
		MTL_CHECK_ELEMS_1(l,1);
		MTL_CHECK_BORDER(l,1,1);
		MTL_CHECK_SIZE(l,1);
		CHECK(*l.insert(l.begin(), v2) == 2);
		MTL_CHECK_ELEMS_2(l,2,1);
		MTL_CHECK_BORDER(l,2,1);
		MTL_CHECK_SIZE(l,2);
		CHECK(*l.insert(l.begin(), v3) == 3);
		MTL_CHECK_ELEMS_3(l,3,2,1);
		MTL_CHECK_BORDER(l,3,1);
		MTL_CHECK_SIZE(l,3);
		l.clear();
		MTL_CHECK_SIZE(l,0);
		CHECK(!v1.is_linked<update_tag>());
		CHECK(!v2.is_linked<update_tag>());
		CHECK(!v3.is_linked<update_tag>());
		CHECK(*l.insert(l.end(), v1) == 1);
		MTL_CHECK_ELEMS_1(l,1);
		MTL_CHECK_BORDER(l,1,1);
		MTL_CHECK_SIZE(l,1);
		CHECK(*l.insert(l.end(), v2) == 2);
		MTL_CHECK_ELEMS_2(l,1,2);
		MTL_CHECK_BORDER(l,1,2);
		MTL_CHECK_SIZE(l,2);
		CHECK(*l.insert(l.end(), v3) == 3);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_BORDER(l,1,3);
		MTL_CHECK_SIZE(l,3);
		l.clear();
		MTL_CHECK_SIZE(l,0);
		CHECK(!v1.is_linked<update_tag>());
		CHECK(!v2.is_linked<update_tag>());
		CHECK(!v3.is_linked<update_tag>());
		CHECK(*l.insert(l.begin(), v1) == 1);
		CHECK(*l.insert(l.end(), v3) == 3);
		CHECK(*l.insert(++l.begin(), v2) == 2);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_BORDER(l,1,3);
		MTL_CHECK_SIZE(l,3);
		l.clear();
		MTL_CHECK_SIZE(l,0);
	}
}

TEST(ilist_insert_period_test)
{
	{
		TList l;
		TList::value_type buf[] = {1,2,3,4,5,6};
		l.insert(l.end(), buf, buf + 3);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		l.clear();
		MTL_CHECK_SIZE(l,0);
		l.insert(l.begin(), buf, buf + 3);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		l.clear();
		MTL_CHECK_SIZE(l,0);
	}
	{
		TList l;
		TList::value_type buf[] = {1,2,3,4,5,6};
		l.insert(l.end(), buf, buf + 3);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_SIZE(l,3);
		l.insert(l.begin(), buf + 3, buf + 6);
		MTL_CHECK_ELEMS_6(l,4,5,6,1,2,3);
		MTL_CHECK_SIZE(l,6);
		l.erase(l.begin(), iterator_advance(l.begin(), 3));
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_SIZE(l,3);
		l.insert(l.end(), buf + 3, buf + 6);
		MTL_CHECK_ELEMS_6(l,1,2,3,4,5,6);
		MTL_CHECK_SIZE(l,6);
		l.erase(iterator_advance(l.begin(), 3), l.end());
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_SIZE(l,3);
		l.insert(iterator_advance(l.begin(), 1), buf + 3, buf + 6);
		MTL_CHECK_ELEMS_6(l,1,4,5,6,2,3);
		MTL_CHECK_SIZE(l,6);
		l.clear();
		MTL_CHECK_SIZE(l,0);
	}
	{
		TList l;
		TList::value_type buf[] = {1,2,3,4,5,6};
		l.insert(l.begin(), buf, buf);
		MTL_CHECK_SIZE(l,0);
		l.insert(l.begin(), buf+1, buf+1);
		MTL_CHECK_SIZE(l,0);
		l.insert(l.end(), buf, buf + 3);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_SIZE(l,3);
		l.insert(l.begin(), buf, buf);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_SIZE(l,3);
		l.insert(l.begin(), buf+2, buf+2);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_SIZE(l,3);
	}
}

TEST(ilist_assign_test)
{
	{
		TList l;
		TList::value_type buf[] = {1,2,3,4,5,6};
		l.assign(buf, buf + 3);
		MTL_CHECK_ELEMS_3(l,1,2,3);
		MTL_CHECK_SIZE(l,3);
		l.assign(buf + 3, buf + 6);
		MTL_CHECK_ELEMS_3(l,4,5,6);
		MTL_CHECK_SIZE(l,3);
		l.assign(buf, buf + 6);
		MTL_CHECK_ELEMS_6(l,1,2,3,4,5,6);
		MTL_CHECK_SIZE(l,6);
	}
}

TEST(ilist_splice_test)
{
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		TList l2(buf + 3, buf + 6);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_ELEMS_3(l2,4,5,6);
		MTL_CHECK_SIZE(l2,3);
		l1.splice(l1.end(), l2);
		MTL_CHECK_ELEMS_6(l1,1,2,3,4,5,6);
		MTL_CHECK_SIZE(l1,6);
		MTL_CHECK_SIZE(l2,0);
	}
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		TList l2(buf + 3, buf + 6);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_ELEMS_3(l2,4,5,6);
		MTL_CHECK_SIZE(l2,3);
		l1.splice(l1.begin(), l2);
		MTL_CHECK_ELEMS_6(l1,4,5,6,1,2,3);
		MTL_CHECK_SIZE(l1,6);
		MTL_CHECK_SIZE(l2,0);
	}
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		TList l2(buf + 3, buf + 6);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_ELEMS_3(l2,4,5,6);
		MTL_CHECK_SIZE(l2,3);
		l1.splice(++(++l1.begin()), l2);
		MTL_CHECK_ELEMS_6(l1,1,2,4,5,6,3);
		MTL_CHECK_SIZE(l1,6);
		MTL_CHECK_SIZE(l2,0);
	}
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		TList l2;
		l1.splice(l1.begin(), l2);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
	}
}

TEST(ilist_splice_elem_test)
{
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		TList l2(buf + 3, buf + 6);
		MTL_CHECK_ELEMS_3(l1,1,2,3);
		MTL_CHECK_SIZE(l1,3);
		MTL_CHECK_ELEMS_3(l2,4,5,6);
		MTL_CHECK_SIZE(l2,3);
		l1.splice(l1.begin(), l2, l2.begin());
		MTL_CHECK_ELEMS_4(l1,4,1,2,3);
		MTL_CHECK_SIZE(l1,4);
		MTL_CHECK_ELEMS_2(l2,5,6);
		MTL_CHECK_SIZE(l2,2);
		l1.splice(++l1.begin(), l2, ++l2.begin());
		MTL_CHECK_ELEMS_5(l1,4,6,1,2,3);
		MTL_CHECK_SIZE(l1,5);
		MTL_CHECK_ELEMS_1(l2,5);
		MTL_CHECK_SIZE(l2,1);
		l1.splice(l1.end(), l2, l2.begin());
		MTL_CHECK_ELEMS_6(l1,4,6,1,2,3,5);
		MTL_CHECK_SIZE(l1,6);
		MTL_CHECK_SIZE(l2,0);
	}
}

TEST(ilist_splice_period_test)
{
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		TList l2(buf + 3, buf + 6);
		l1.splice(l1.begin(), l2, l2.begin(), ++(++l2.begin()));
		MTL_CHECK_ELEMS_5(l1,4,5,1,2,3);
		MTL_CHECK_SIZE(l1,5);
		MTL_CHECK_ELEMS_1(l2,6);
		MTL_CHECK_SIZE(l2,1);
	}
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		TList l2(buf + 3, buf + 6);
		l1.splice(l1.end(), l2, l2.begin(), ++(++l2.begin()));
		MTL_CHECK_ELEMS_5(l1,1,2,3,4,5);
		MTL_CHECK_SIZE(l1,5);
		MTL_CHECK_ELEMS_1(l2,6);
		MTL_CHECK_SIZE(l2,1);
	}
	{
		TList::value_type buf[] = {1,2,3,4,5,6};
		TList l1(buf, buf + 3);
		TList l2(buf + 3, buf + 6);
		l1.splice(++l1.begin(), l2, ++l2.begin(), ++(++l2.begin()));
		MTL_CHECK_ELEMS_4(l1,1,5,2,3);
		MTL_CHECK_SIZE(l1,4);
		MTL_CHECK_ELEMS_2(l2,4,6);
		MTL_CHECK_SIZE(l2,2);
		l1.splice(--l1.end(), l2, l2.begin(), ++(++l2.begin()));
		MTL_CHECK_ELEMS_6(l1,1,5,2,4,6,3);
		MTL_CHECK_SIZE(l1,6);
		MTL_CHECK_SIZE(l2,0);
	}
}

TEST(ilist_erase_test)
{
	{
		TList l;
		fill_n_list(l,5);
		MTL_CHECK_ELEMS_5(l,1,2,3,4,5);
		MTL_CHECK_BORDER(l,1,5);
		MTL_CHECK_SIZE(l,5);
		CHECK(*l.erase(l.begin()) == 2);
		MTL_CHECK_ELEMS_4(l,2,3,4,5);
		MTL_CHECK_BORDER(l,2,5);
		MTL_CHECK_SIZE(l,4);
		CHECK(*l.erase(iterator_advance(l.begin(),1)) == 4);
		MTL_CHECK_ELEMS_3(l,2,4,5);
		MTL_CHECK_BORDER(l,2,5);
		MTL_CHECK_SIZE(l,3);
		CHECK(l.erase(iterator_advance(l.begin(),2)) == l.end());
		MTL_CHECK_ELEMS_2(l,2,4);
		MTL_CHECK_BORDER(l,2,4);
		MTL_CHECK_SIZE(l,2);
		CHECK(l.erase(--l.end()) == l.end());
		MTL_CHECK_ELEMS_1(l,2);
		MTL_CHECK_BORDER(l,2,2);
		MTL_CHECK_SIZE(l,1);
		CHECK(l.erase(--l.end()) == l.end());
		MTL_CHECK_SIZE(l,0);
	}
}

TEST(ilist_erase_period_test)
{
	{
		TList l;
		fill_n_list(l,5);
		MTL_CHECK_ELEMS_5(l,1,2,3,4,5);
		MTL_CHECK_BORDER(l,1,5);
		MTL_CHECK_SIZE(l,5);
		CHECK(*l.erase(l.begin(), iterator_advance(l.begin(),2)) == 3);
		MTL_CHECK_ELEMS_3(l,3,4,5);
		MTL_CHECK_BORDER(l,3,5);
		MTL_CHECK_SIZE(l,3);
		CHECK(l.erase(iterator_advance(l.begin(),1), l.end()) == l.end());
		MTL_CHECK_ELEMS_1(l,3);
		MTL_CHECK_BORDER(l,3,3);
		MTL_CHECK_SIZE(l,1);
		CHECK(l.erase(--l.end(), l.end()) == l.end());
		MTL_CHECK_SIZE(l,0);
	}
}

TEST(ilist_iterator_to_test)
{
	{
		TList l;
		TList::value_type v1(1);
		TList::value_type v2(2);
		l.push_back(v1);
		l.push_back(v2);
		{
			TList::value_type& ncv1 = v1;
			TList::value_type& ncv2 = v2;
			CHECK(0 == iterator_distance(l. begin(), TList::iterator_to(ncv1)));
			CHECK(1 == iterator_distance(l. begin(), TList::iterator_to(ncv2)));
		}
		{
			const TList::value_type& cv1 = v1;
			const TList::value_type& cv2 = v2;
			CHECK(0 == iterator_distance(l.cbegin(), TList::iterator_to(cv1)));
			CHECK(1 == iterator_distance(l.cbegin(), TList::iterator_to(cv2)));
		}
	}
}
