#include "tests.hpp"

// ----------------------------------------------------------------------------
// 
// test class
// 
// ----------------------------------------------------------------------------

struct TestExcIntClass
{
	int _i;
	static bool emit_throw;
	static size_t destr_num;
	static size_t constr_num;
	
	static void rnd_throw()
	{
		if ( emit_throw )
		{
			if ( 0 == tests_random::gen_int(0,100) )
				throw 1;
		}
	}
	
	static void rnd_enable(bool yesno)
	{
		emit_throw = yesno;
	}
	
	TestExcIntClass(int i = 0)
	: _i(i)
	{
		rnd_throw();
		++constr_num;
	}
	
	TestExcIntClass(const TestExcIntClass& other)
	: _i(other._i)
	{
		rnd_throw();
		++constr_num;
	}
	
	TestExcIntClass& operator=(const TestExcIntClass& other)
	{
		if ( this != &other )
		{
			_i = other._i;
			rnd_throw();
		}
		return *this;
	}
	
	~TestExcIntClass()
	{
		++destr_num;
	}
	
	bool operator<(const TestExcIntClass& other) const{
		return _i < other._i;
	}
	
	bool operator == (int i) const { return _i == i; }
	bool operator != (int i) const { return _i != i; }
	bool operator == (const TestExcIntClass& other) const { return _i == other._i; }
	bool operator != (const TestExcIntClass& other) const { return _i != other._i; }
};
bool   TestExcIntClass::emit_throw = true;
size_t TestExcIntClass::destr_num  = 0;
size_t TestExcIntClass::constr_num = 0;

// ----------------------------------------------------------------------------
// 
// TestAllocator
// 
// ----------------------------------------------------------------------------

struct TestAllocator
{
	int _tag;
	static size_t allocate_size;
	static size_t deallocate_size;
	
	TestAllocator(int tag = 0)
	: _tag(tag)
	{}
	
	TestAllocator(const TestAllocator& other)
	: _tag(other._tag)
	{}
	
	TestAllocator& operator = (const TestAllocator& other)
	{
		if ( this != &other )
			_tag = other._tag;
		return *this;
	}
	
	~TestAllocator()
	{}
	
	bool operator == (const TestAllocator& other)
	{
		return _tag == other._tag;
	}
	
	bool operator != (const TestAllocator& other)
	{
		return _tag != other._tag;
	}
	
	void* allocate(size_t size)
	{
		void* mem = MTL_malloc(size);
		if ( !mem )
			throw std::bad_alloc();
		allocate_size += size;
		return mem;
	}
	
	void deallocate(void* ptr, size_t size)
	{
		MTL_free(ptr);
		deallocate_size += size;
	}
};
size_t TestAllocator::allocate_size   = 0;
size_t TestAllocator::deallocate_size = 0;

//typedef mtl::array<int> TIntArr;
typedef mtl::array<TestExcIntClass, TestAllocator> TIntArr;

// ----------------------------------------------------------------------------
// 
// helpers
// 
// ----------------------------------------------------------------------------

template < typename TA >
static void fill_n_vec(TA& a, size_t n){
	a.resize(n);
	for ( size_t i = 0; i < n; ++i ) {
		a[i] = i + 1;
	}
}

template < typename TA >
static void fill_rn_vec(TA& a, size_t n){
	a.reserve(n);
	for ( size_t i = 0; i < n; ++i ) {
		a.push_back(n-i);
	}
}

template < typename T >
static void check_size_pow_of_two(const T& arr){
	const size_t pow_two[] = {1,2,4,8,16,32,64,128};
	const size_t pow_two_size = sizeof(pow_two)/sizeof(pow_two[0]);
	for ( size_t i = 0; i < pow_two_size; ++i ) {
		if ( arr.size() <= pow_two[i] ) {
			CHECK(arr.capacity() == pow_two[i]);
			break;
		}
	}
}

#define MTL_TRY_LOOP_BEGIN()\
	for ( size_t mtl_try_loop_count = 0; mtl_try_loop_count < 3000; ++mtl_try_loop_count )\
	{ if ( mtl_try_loop_count == 0 ) {::printf("."); tests_random::set_seed(::time(NULL)); }\
		try {

#define MTL_TRY_LOOP_END()\
		} catch(int) {\
		MTL_CHECK_CLASS_AND_ALLOC_EXT()\
	} MTL_CHECK_CLASS_AND_ALLOC() }

#define MTL_CHECK_CLASS_AND_ALLOC()\
	CHECK(TestExcIntClass::constr_num == TestExcIntClass::destr_num);\
	CHECK(TestAllocator::allocate_size == TestAllocator::deallocate_size);\
	TestExcIntClass::constr_num = TestExcIntClass::destr_num = 0;\
	TestAllocator::allocate_size = TestAllocator::deallocate_size = 0;
	
#define MTL_CHECK_CLASS_AND_ALLOC_EXT()\
	CHECK(TestExcIntClass::constr_num == TestExcIntClass::destr_num);\
	CHECK(TestAllocator::allocate_size == TestAllocator::deallocate_size);\
	TestExcIntClass::constr_num = TestExcIntClass::destr_num = 0;\
	TestAllocator::allocate_size = TestAllocator::deallocate_size = 0;

#define MTL_CHECK_TEST_RESET()\
	TestExcIntClass::constr_num = TestExcIntClass::destr_num = 0;\
	TestAllocator::allocate_size = TestAllocator::deallocate_size = 0;

#define MTL_CHECK_FULL_EMPTY(_arr)\
{\
	CHECK(_arr.data ()    == NULL);\
	CHECK(_arr.cdata()    == NULL);\
	CHECK(_arr.size()     == 0);\
	CHECK(_arr.capacity() == 0);\
	CHECK(_arr.empty()    == true);\
}

#define MTL_CHECK_SIZE(_arr, _size, _capacity)\
{\
	CHECK(_arr.size()     == _size);\
	CHECK(_arr.capacity() == _capacity);\
}

#define MTL_CHECK_BORDER(_arr, _front, _back)\
{\
	CHECK(_arr.front() == _front);\
	CHECK(_arr.back()  == _back);\
}

#define MTL_CHECK_ARRAY_EQUAL(_arr1, _arr2)\
{\
	CHECK(_arr1.size() == _arr2.size());\
	for ( size_t i = 0; i < _arr1.size(); ++i )\
		CHECK(_arr1[i] == _arr2[i]);\
}

#define MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(_arr1, _arr2)\
{\
	MTL_CHECK_ARRAY_EQUAL(_arr1, _arr2)\
	CHECK(_arr1.capacity() == _arr2.capacity());\
}

#define MTL_CHECK_ELEMS_1(_arr, _e0)\
	CHECK(_arr[0] == _e0);

#define MTL_CHECK_ELEMS_2(_arr, _e0, _e1)\
	MTL_CHECK_ELEMS_1(_arr, _e0);\
	CHECK(_arr[1] == _e1);

#define MTL_CHECK_ELEMS_3(_arr, _e0, _e1, _e2)\
	MTL_CHECK_ELEMS_2(_arr, _e0, _e1);\
	CHECK(_arr[2] == _e2);

#define MTL_CHECK_ELEMS_4(_arr, _e0, _e1, _e2, _e3)\
	MTL_CHECK_ELEMS_3(_arr, _e0, _e1, _e2);\
	CHECK(_arr[3] == _e3);

#define MTL_CHECK_ELEMS_5(_arr, _e0, _e1, _e2, _e3, _e4)\
	MTL_CHECK_ELEMS_4(_arr, _e0, _e1, _e2, _e3);\
	CHECK(_arr[4] == _e4);

#define MTL_CHECK_ELEMS_6(_arr, _e0, _e1, _e2, _e3, _e4, _e5)\
	MTL_CHECK_ELEMS_5(_arr, _e0, _e1, _e2, _e3, _e4);\
	CHECK(_arr[5] == _e5);

#define MTL_CHECK_ELEMS_7(_arr, _e0, _e1, _e2, _e3, _e4, _e5, _e6)\
	MTL_CHECK_ELEMS_6(_arr, _e0, _e1, _e2, _e3, _e4, _e5);\
	CHECK(_arr[6] == _e6);

#define MTL_CHECK_ELEMS_8(_arr, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7)\
	MTL_CHECK_ELEMS_7(_arr, _e0, _e1, _e2, _e3, _e4, _e5, _e6);\
	CHECK(_arr[7] == _e7);

#define MTL_CHECK_ELEMS_9(_arr, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7, _e8)\
	MTL_CHECK_ELEMS_8(_arr, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7);\
	CHECK(_arr[8] == _e8);

#define MTL_CHECK_ELEMS_10(_arr, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7, _e8, _e9)\
	MTL_CHECK_ELEMS_9(_arr, _e0, _e1, _e2, _e3, _e4, _e5, _e6, _e7, _e8);\
	CHECK(_arr[9] == _e9);

// ----------------------------------------------------------------------------
// 
// tests
// 
// ----------------------------------------------------------------------------

#define TEST_0
//#define TEST_1
//#define TEST_2
//#define TEST_3
//#define TEST_4
//#define TEST_5
//#define TEST_6
//#define TEST_7
//#define TEST_8
//#define TEST_9
//#define TEST_10
//#define TEST_11
//#define TEST_12
//#define TEST_13
//#define TEST_14
//#define TEST_15

// ----------------------------------------------------------------------------
// 
// operators compile test
// 
// ----------------------------------------------------------------------------

#ifdef TEST_0
TEST(array_operators_compile_test)
{
	TIntArr i1;
	TIntArr i2;
	i1 == i2;
	i1 != i2;
	i1 <  i2;
	i1 <= i2;
	i1 >  i2;
	i1 >= i2;
}
#endif

// ----------------------------------------------------------------------------
// 
// empty_constructors
// 
// ----------------------------------------------------------------------------

#ifdef TEST_1
TEST(array_empty_constructors){
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		MTL_CHECK_FULL_EMPTY(arr)
		TIntArr arr2 = arr;
		MTL_CHECK_FULL_EMPTY(arr2)
		MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(arr,arr2)
		CHECK(arr2.get_allocator() == arr.get_allocator());
		TIntArr arr3(0);
		MTL_CHECK_FULL_EMPTY(arr3)
		TIntArr arr4 = arr3;
		MTL_CHECK_FULL_EMPTY(arr3)
		MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(arr3,arr4)
		CHECK(arr4.get_allocator() == arr3.get_allocator());
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// fill_elems_constructors
// 
// ----------------------------------------------------------------------------

#ifdef TEST_2
TEST(array_fill_elems_constructors){
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr(5,123);
		MTL_CHECK_SIZE(arr,5,5);
		MTL_CHECK_BORDER(arr,123,123)
		MTL_CHECK_ELEMS_5(arr,123,123,123,123,123)
		TIntArr arr2 = arr;
		MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(arr,arr2)
	MTL_TRY_LOOP_END()
	
	MTL_TRY_LOOP_BEGIN()
		size_t n = size_t(tests_random::gen_int(1,30));
		int v = tests_random::gen_int(-1000,1000);
		TIntArr arr(n,v);
		MTL_CHECK_SIZE(arr,n,n);
		MTL_CHECK_BORDER(arr,v,v)
		for ( size_t i = 0; i < n; ++i )
			CHECK(arr[i] == v);
		TIntArr arr2 = arr;
		MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(arr,arr2)
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// fill_elems_period_constructors
// 
// ----------------------------------------------------------------------------

#ifdef TEST_3
TEST(array_fill_elems_period_constructors){
	MTL_TRY_LOOP_BEGIN()
		TIntArr::value_type buf[] = {1,2,3,4,5};
		TIntArr arr(buf, buf + sizeof(buf)/sizeof(buf[0]));
		MTL_CHECK_ELEMS_5(arr,1,2,3,4,5);
	MTL_TRY_LOOP_END()
	
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr,5);
		TIntArr arr2(&arr[0], &arr[4]);
		MTL_CHECK_ELEMS_4(arr2,1,2,3,4);
		MTL_CHECK_SIZE(arr2,4,4);
		MTL_CHECK_BORDER(arr2,1,4);
		TIntArr arr3 = arr2;
		MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(arr2,arr3);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// assignment_operator
// 
// ----------------------------------------------------------------------------

#ifdef TEST_4
TEST(array_assignment_operator){
	const size_t arr_size = 5;
	
	// self_capacity < arr_size
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr, arr_size);
		for ( size_t i = 0; i < arr.size(); ++i ) {
			for ( size_t j = 0; j < i; ++j ) {
				TIntArr a;
				fill_rn_vec(a,i);
				for ( size_t k = 0; k < j; ++k ) {
					a.pop_back();
				}
				a = arr;
				MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(arr,a)
			}
		}
	MTL_TRY_LOOP_END()
	// self_size < arr_size
	// self_capacity >= arr_size
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr, arr_size);
		for ( size_t i = arr.size(), ie = arr.size() * 2; i < ie; ++i ) {
			for ( size_t j = 0, je = i; j < je; ++j ) {
				TIntArr a;
				fill_rn_vec(a,i);
				for ( size_t k = 0; k < j; ++k ) {
					a.pop_back();
				}
				size_t old_capacity = a.capacity();
				a = arr;
				MTL_CHECK_ARRAY_EQUAL(arr,a)
				MTL_CHECK_SIZE(a, arr.size(), old_capacity)
			}
		}
	MTL_TRY_LOOP_END()
	// self_size >= arr_size
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr, arr_size);
		for ( size_t i = arr.size(), ie = arr.size() * 2; i < ie; ++i ) {
			for ( size_t j = 0, je = i - arr.size() + 1; j < je; ++j ) {
				TIntArr a;
				fill_rn_vec(a,i);
				for ( size_t k = 0; k < j; ++k ) {
					a.pop_back();
				}
				a = arr;
				MTL_CHECK_ARRAY_EQUAL(arr,a)
			}
		}
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// get_elem_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_5
TEST(array_get_elem_func){
	MTL_TRY_LOOP_BEGIN()
		for ( size_t i = 1; i < 100; ++i ) {
			TIntArr a(i,123);
			for ( size_t j = 0; j < i; ++j ) {
				a[j] = static_cast<int>(j);
			}
			for ( size_t j = 0; j < i; ++j ) {
				CHECK(a[j]    == static_cast<int>(j));
				CHECK(a.at(j) == static_cast<int>(j));
				CHECK(a[j]    == *(a.data() + j));
			}
		}
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// pop_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_6
TEST(array_pop_push_func){
	MTL_TRY_LOOP_BEGIN()
		for ( size_t i = 0; i < 100; ++i ) {
			TIntArr a;
			fill_n_vec(a, i);
			size_t size = i;
			while ( !a.empty() ) {
				--size;
				a.pop_back();
				CHECK(a.size() == size);
				CHECK(a.capacity() == i);
				for ( size_t j = 0; j < a.size(); ++j )
					CHECK(a[j] == static_cast<int>(j + 1));
			}
		}
	MTL_TRY_LOOP_END()
	
	MTL_TRY_LOOP_BEGIN()
		TIntArr a;
		for ( size_t i = 0; i < 100; ++i ) {
			a.push_back(i);
			CHECK(a.size() == i + 1);
			for ( size_t j = 0; j < a.size(); ++j ) {
				CHECK(a[j] == static_cast<int>(j));
			}
			check_size_pow_of_two(a);
		}
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// erase_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_7
TEST(array_erase_func){
	MTL_TRY_LOOP_BEGIN()
		TIntArr a;
		fill_n_vec(a, 10);
		MTL_CHECK_SIZE(a,10,10);
		MTL_CHECK_ELEMS_10(a,1,2,3,4,5,6,7,8,9,10);
		a.erase(0);
		MTL_CHECK_SIZE(a,9,10);
		MTL_CHECK_ELEMS_9(a,2,3,4,5,6,7,8,9,10);
		a.erase(1);
		MTL_CHECK_SIZE(a,8,10);
		MTL_CHECK_ELEMS_8(a,2,4,5,6,7,8,9,10);
		a.erase(2);
		MTL_CHECK_SIZE(a,7,10);
		MTL_CHECK_ELEMS_7(a,2,4,6,7,8,9,10);
		a.erase(3);
		MTL_CHECK_SIZE(a,6,10);
		MTL_CHECK_ELEMS_6(a,2,4,6,8,9,10);
		a.erase(4);
		MTL_CHECK_SIZE(a,5,10);
		MTL_CHECK_ELEMS_5(a,2,4,6,8,10);
		//
		a.erase(0);
		MTL_CHECK_SIZE(a,4,10);
		MTL_CHECK_ELEMS_4(a,4,6,8,10);
		a.erase(0);
		MTL_CHECK_SIZE(a,3,10);
		MTL_CHECK_ELEMS_3(a,6,8,10);
		a.erase(0);
		MTL_CHECK_SIZE(a,2,10);
		MTL_CHECK_ELEMS_2(a,8,10);
		a.erase(0);
		MTL_CHECK_SIZE(a,1,10);
		MTL_CHECK_ELEMS_1(a,10);
		a.erase(0);
		MTL_CHECK_SIZE(a,0,10);
	MTL_TRY_LOOP_END()
	
	MTL_TRY_LOOP_BEGIN()
		TIntArr a;
		fill_n_vec(a, 10);
		MTL_CHECK_SIZE(a,10,10);
		MTL_CHECK_ELEMS_10(a,1,2,3,4,5,6,7,8,9,10);
		a.erase(a.size()-1);
		MTL_CHECK_SIZE(a,9,10);
		MTL_CHECK_ELEMS_9(a,1,2,3,4,5,6,7,8,9);
		a.erase(a.size()-2);
		MTL_CHECK_SIZE(a,8,10);
		MTL_CHECK_ELEMS_8(a,1,2,3,4,5,6,7,9);
		a.erase(a.size()-3);
		MTL_CHECK_SIZE(a,7,10);
		MTL_CHECK_ELEMS_7(a,1,2,3,4,5,7,9);
		a.erase(a.size()-4);
		MTL_CHECK_SIZE(a,6,10);
		MTL_CHECK_ELEMS_6(a,1,2,3,5,7,9);
		a.erase(a.size()-5);
		MTL_CHECK_SIZE(a,5,10);
		MTL_CHECK_ELEMS_5(a,1,3,5,7,9);
		//
		a.erase(a.size()-1);
		MTL_CHECK_SIZE(a,4,10);
		MTL_CHECK_ELEMS_4(a,1,3,5,7);
		a.erase(a.size()-1);
		MTL_CHECK_SIZE(a,3,10);
		MTL_CHECK_ELEMS_3(a,1,3,5);
		a.erase(a.size()-1);
		MTL_CHECK_SIZE(a,2,10);
		MTL_CHECK_ELEMS_2(a,1,3);
		a.erase(a.size()-1);
		MTL_CHECK_SIZE(a,1,10);
		MTL_CHECK_ELEMS_1(a,1);
		a.erase(a.size()-1);
		MTL_CHECK_SIZE(a,0,10);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// erase_period_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_8
TEST(array_erase_period_func){
	MTL_TRY_LOOP_BEGIN()
		TIntArr a;
		fill_n_vec(a, 10);
		MTL_CHECK_SIZE(a,10,10);
		MTL_CHECK_ELEMS_10(a,1,2,3,4,5,6,7,8,9,10);
		a.erase(0,2);
		MTL_CHECK_SIZE(a,8,10);
		MTL_CHECK_ELEMS_8(a,3,4,5,6,7,8,9,10);
		a.erase(a.size()-2,a.size());
		MTL_CHECK_SIZE(a,6,10);
		MTL_CHECK_ELEMS_6(a,3,4,5,6,7,8);
		a.erase(1,3);
		MTL_CHECK_SIZE(a,4,10);
		MTL_CHECK_ELEMS_4(a,3,6,7,8);
		a.erase(0, a.size());
		MTL_CHECK_SIZE(a,0,10);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// insert_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_9
TEST(array_insert_func){
	// insert to back
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		for ( size_t i = 0; i < 15; ++i ) {
			arr.insert(arr.size(), i);
			arr.insert(arr.size(), arr[arr.size()-1]);
			arr.pop_back();
		}
		MTL_CHECK_SIZE(arr,15,16);
		for ( size_t i = 0; i < arr.size(); ++i ) {
			CHECK(arr[i] == static_cast<int>(i));
		}
	MTL_TRY_LOOP_END()
	
	// insert with capacity
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr, 5);
		arr.erase(0, arr.size());
		MTL_CHECK_SIZE(arr,0,5);
		arr.insert(0,0);
		MTL_CHECK_SIZE(arr,1,5);
		MTL_CHECK_ELEMS_1(arr,0);
		arr.insert(0,arr[0]);
		MTL_CHECK_SIZE(arr,2,5);
		MTL_CHECK_ELEMS_2(arr,0,0);
		arr.insert(1,1);
		MTL_CHECK_SIZE(arr,3,5);
		MTL_CHECK_ELEMS_3(arr,0,1,0);
		arr.insert(1,arr[2]);
		MTL_CHECK_SIZE(arr,4,5);
		MTL_CHECK_ELEMS_4(arr,0,0,1,0);
	MTL_TRY_LOOP_END()
	
	// insert without capacity
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		arr.insert(0,0);
		MTL_CHECK_SIZE(arr,1,1);
		MTL_CHECK_ELEMS_1(arr,0);
		arr.insert(0,arr[0]);
		MTL_CHECK_SIZE(arr,2,2);
		MTL_CHECK_ELEMS_2(arr,0,0);
		arr.insert(1,1);
		MTL_CHECK_SIZE(arr,3,4);
		MTL_CHECK_ELEMS_3(arr,0,1,0);
		arr.insert(1,arr[1]);
		MTL_CHECK_SIZE(arr,4,4);
		MTL_CHECK_ELEMS_4(arr,0,1,1,0);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// insert_period_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_10
TEST(array_insert_period_func){
	// insert without capacity
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		arr.insert(0,2,5);
		MTL_CHECK_SIZE(arr,2,2);
		MTL_CHECK_ELEMS_2(arr,5,5);
		arr.insert(arr.size(),2,10);
		MTL_CHECK_SIZE(arr,4,4);
		MTL_CHECK_ELEMS_4(arr,5,5,10,10);
		arr.insert(2,2,7);
		MTL_CHECK_SIZE(arr,6,8);
		MTL_CHECK_ELEMS_6(arr,5,5,7,7,10,10);
	MTL_TRY_LOOP_END()
	
	// insert with capacity
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		arr.reserve(10);
		MTL_CHECK_SIZE(arr,0,10);
		arr.insert(0,5,1);
		MTL_CHECK_SIZE(arr,5,10);
		MTL_CHECK_ELEMS_5(arr,1,1,1,1,1);
		arr.insert(1,3,2);
		MTL_CHECK_SIZE(arr,8,10);
		MTL_CHECK_ELEMS_8(arr,1,2,2,2,1,1,1,1);
		arr.insert(arr.size(),2,3);
		MTL_CHECK_SIZE(arr,10,10);
		MTL_CHECK_ELEMS_10(arr,1,2,2,2,1,1,1,1,3,3);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// insert_period2_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_11
TEST(array_insert_period2_func){
	// insert without capacity
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		{
			TIntArr::value_type buf[] = {4,5};
			arr.insert(0,buf,buf+sizeof(buf)/sizeof(buf[0]));
			MTL_CHECK_SIZE(arr,2,2);
			MTL_CHECK_ELEMS_2(arr,4,5);
			MTL_CHECK_BORDER(arr,4,5);
		}
		{
			TIntArr::value_type buf[] = {9,10};
			arr.insert(arr.size(),buf,buf+sizeof(buf)/sizeof(buf[0]));
			MTL_CHECK_SIZE(arr,4,4);
			MTL_CHECK_ELEMS_4(arr,4,5,9,10);
			MTL_CHECK_BORDER(arr,4,10);
		}
		{
			TIntArr::value_type buf[2] = {6,7};
			arr.insert(2,buf,buf+sizeof(buf)/sizeof(buf[0]));
			MTL_CHECK_SIZE(arr,6,8);
			MTL_CHECK_ELEMS_6(arr,4,5,6,7,9,10);
			MTL_CHECK_BORDER(arr,4,10);
		}
	MTL_TRY_LOOP_END()
	
	// insert with capacity
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		arr.reserve(10);
		MTL_CHECK_SIZE(arr,0,10);
		{
			TIntArr::value_type buf[] = {10,11,12,13,14};
			arr.insert(0,buf,buf+sizeof(buf)/sizeof(buf[0]));
			MTL_CHECK_SIZE(arr,5,10);
			MTL_CHECK_ELEMS_5(arr,10,11,12,13,14);
			MTL_CHECK_BORDER(arr,10,14);
		}
		{
			TIntArr::value_type buf[] = {1,2,3};
			arr.insert(1,buf,buf+sizeof(buf)/sizeof(buf[0]));
			MTL_CHECK_SIZE(arr,8,10);
			MTL_CHECK_ELEMS_8(arr,10,1,2,3,11,12,13,14);
			MTL_CHECK_BORDER(arr,10,14);
		}
		{
			TIntArr::value_type buf[] = {4,5};
			arr.insert(arr.size(),buf,buf+sizeof(buf)/sizeof(buf[0]));
			MTL_CHECK_SIZE(arr,10,10);
			MTL_CHECK_ELEMS_10(arr,10,1,2,3,11,12,13,14,4,5);
			MTL_CHECK_BORDER(arr,10,5);
		}
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// reserve_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_12
TEST(array_reserve_func){
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		arr.reserve(10);
		MTL_CHECK_SIZE(arr,0,10);
		arr.reserve(5);
		MTL_CHECK_SIZE(arr,0,10);
		arr.reset();
		MTL_CHECK_FULL_EMPTY(arr);
	MTL_TRY_LOOP_END()
	
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr,5);
		MTL_CHECK_SIZE(arr,5,5);
		arr.reserve(10);
		MTL_CHECK_SIZE(arr,5,10);
		MTL_CHECK_ELEMS_5(arr,1,2,3,4,5);
		arr.reset();
		MTL_CHECK_FULL_EMPTY(arr);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// resize_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_13
TEST(array_resize_func){
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr,5);
		MTL_CHECK_SIZE(arr,5,5);
		MTL_CHECK_ELEMS_5(arr,1,2,3,4,5);
		arr.resize(10,6);
		MTL_CHECK_SIZE(arr,10,10);
		MTL_CHECK_ELEMS_10(arr,1,2,3,4,5,6,6,6,6,6);
		arr.resize(2);
		MTL_CHECK_SIZE(arr,2,10);
		MTL_CHECK_ELEMS_2(arr,1,2);
		arr.resize(4,5);
		MTL_CHECK_SIZE(arr,4,10);
		MTL_CHECK_ELEMS_4(arr,1,2,5,5);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// shrink_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_14
TEST(array_shrink_func){
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		MTL_CHECK_FULL_EMPTY(arr);
		arr.shrink();
		MTL_CHECK_FULL_EMPTY(arr);
		fill_n_vec(arr,5);
		MTL_CHECK_SIZE(arr,5,5);
		MTL_CHECK_ELEMS_5(arr,1,2,3,4,5);
		TIntArr arr2 = arr;
		arr2.shrink();
		MTL_CHECK_ARRAY_EQUAL_WITH_CAPACITY(arr,arr2);
	MTL_TRY_LOOP_END()
	
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr;
		fill_n_vec(arr,10);
		arr.erase(5,arr.size());
		MTL_CHECK_SIZE(arr,5,10);
		MTL_CHECK_ELEMS_5(arr,1,2,3,4,5);
		arr.shrink();
		MTL_CHECK_SIZE(arr,5,5);
		MTL_CHECK_ELEMS_5(arr,1,2,3,4,5);
		arr.clear();
		MTL_CHECK_SIZE(arr,0,5);
		arr.shrink();
		MTL_CHECK_FULL_EMPTY(arr);
	MTL_TRY_LOOP_END()
}
#endif

// ----------------------------------------------------------------------------
// 
// swap_func
// 
// ----------------------------------------------------------------------------

#ifdef TEST_15
TEST(array_swap_func){
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr1;
		fill_n_vec(arr1,5);
		TIntArr arr2;
		fill_rn_vec(arr2,10);
		arr1.swap(arr2);
		MTL_CHECK_SIZE(arr1,10,10);
		MTL_CHECK_ELEMS_10(arr1,10,9,8,7,6,5,4,3,2,1);
		MTL_CHECK_SIZE(arr2,5,5);
		MTL_CHECK_ELEMS_5(arr2,1,2,3,4,5);
	MTL_TRY_LOOP_END()
	
	MTL_TRY_LOOP_BEGIN()
		TIntArr arr1;
		fill_n_vec(arr1,2);
		arr1.reserve(10);
		TIntArr arr2;
		fill_rn_vec(arr2,4);
		arr2.reserve(20);
		arr1.swap(arr2);
		MTL_CHECK_SIZE(arr1,4,20);
		MTL_CHECK_ELEMS_4(arr1,4,3,2,1);
		MTL_CHECK_SIZE(arr2,2,10);
		MTL_CHECK_ELEMS_2(arr2,1,2);
	MTL_TRY_LOOP_END()
}
#endif
