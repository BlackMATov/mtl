#pragma once
#include <mtl/array.hpp>
#include <mtl/ilist.hpp>
#include "../UnitTest++/UnitTest++.h"

// ----------------------------------------------------------------------------
// 
// tests_random
// 
// ----------------------------------------------------------------------------

namespace tests_random
{
	static unsigned int g_seed = 0;
	inline void set_seed(unsigned int seed)
	{
		g_seed = seed;
	}
	inline int gen_int(int min, int max)
	{
		g_seed = 214013 * g_seed + 2531011;
		return min + (g_seed ^ g_seed >> 15) % (max - min + 1);
	}
}
